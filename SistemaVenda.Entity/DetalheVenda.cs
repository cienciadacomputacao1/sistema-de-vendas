﻿namespace SistemaVenda.Entity
{
    public partial class DetalheVenda
    {
        public int DetalheVendaId { get; set; }
        public int? VendaId { get; set; }
        public int? ProdutoId { get; set; }
        public string? MarcaProduto { get; set; }
        public string? DescricaoProduto { get; set; }
        public string? CategoriaProduto { get; set; }
        public int? Quantidade { get; set; }
        public decimal? Preco { get; set; }
        public decimal? Total { get; set; }

        public virtual Venda? Venda { get; set; }
    }
}
