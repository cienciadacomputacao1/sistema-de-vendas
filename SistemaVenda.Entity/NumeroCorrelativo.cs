﻿namespace SistemaVenda.Entity
{
    public partial class NumeroCorrelativo
    {
        public int NumeroCorrelativoId { get; set; }
        public int? UltimoNumero { get; set; }
        public int? QuantidadeDigitos { get; set; }
        public string? Gestao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
    }
}
