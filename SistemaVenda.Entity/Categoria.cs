﻿namespace SistemaVenda.Entity
{
    public partial class Categoria
    {
        public Categoria()
        {
            Produtos = new HashSet<Produto>();
        }

        public int CategoriaId { get; set; }
        public string? Descricao { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual ICollection<Produto> Produtos { get; set; }
    }
}
