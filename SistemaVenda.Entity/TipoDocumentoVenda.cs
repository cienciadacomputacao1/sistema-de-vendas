﻿namespace SistemaVenda.Entity
{
    public partial class TipoDocumentoVenda
    {
        public TipoDocumentoVenda()
        {
            Venda = new HashSet<Venda>();
        }

        public int TipoDocumentoVendaId { get; set; }
        public string? Descricao { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual ICollection<Venda> Venda { get; set; }
    }
}
