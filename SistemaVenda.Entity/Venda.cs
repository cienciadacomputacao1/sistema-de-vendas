﻿namespace SistemaVenda.Entity
{
    public partial class Venda
    {
        public Venda()
        {
            DetalheVenda = new HashSet<DetalheVenda>();
        }

        public int VendaId { get; set; }
        public string? NumeroVenda { get; set; }
        public int? TipoDocumentoVendaId { get; set; }
        public int? UsuarioId { get; set; }
        public string? DocumentoCliente { get; set; }
        public string? NomeCliente { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? ImpostoTotal { get; set; }
        public decimal? Total { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual TipoDocumentoVenda? TipoDocumentoVenda { get; set; }
        public virtual Usuario? Usuario { get; set; }
        public virtual ICollection<DetalheVenda> DetalheVenda { get; set; }
    }
}
