﻿namespace SistemaVenda.Entity
{
    public partial class Menu
    {
        public Menu()
        {
            InverseMenuPai = new HashSet<Menu>();
            RoleMenus = new HashSet<RoleMenu>();
        }

        public int MenuId { get; set; }
        public string? Descricao { get; set; }
        public int? MenuPaiId { get; set; }
        public string? Icone { get; set; }
        public string? Controlador { get; set; }
        public string? PaginaAcao { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual Menu? MenuPai { get; set; }
        public virtual ICollection<Menu> InverseMenuPai { get; set; }
        public virtual ICollection<RoleMenu> RoleMenus { get; set; }
    }
}
