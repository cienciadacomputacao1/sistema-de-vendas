﻿namespace SistemaVenda.Entity
{
    public partial class Role
    {
        public Role()
        {
            RoleMenus = new HashSet<RoleMenu>();
            Usuarios = new HashSet<Usuario>();
        }

        public int RoleId { get; set; }
        public string? Descricao { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual ICollection<RoleMenu> RoleMenus { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
