﻿namespace SistemaVenda.Entity
{
    public partial class Produto
    {
        public int ProdutoId { get; set; }
        public string? CodigoBarra { get; set; }
        public string? Marca { get; set; }
        public string? Descricao { get; set; }
        public int? CategoriaId { get; set; }
        public int? Estoque { get; set; }
        public string? UrlImagem { get; set; }
        public string? NomeImagem { get; set; }
        public decimal? Preco { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual Categoria? Categoria { get; set; }
    }
}
