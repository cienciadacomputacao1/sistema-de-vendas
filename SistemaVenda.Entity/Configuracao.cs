﻿namespace SistemaVenda.Entity
{
    public partial class Configuracao
    {
        public string? Recurso { get; set; }
        public string? Propriedade { get; set; }
        public string? Valor { get; set; }
    }
}
