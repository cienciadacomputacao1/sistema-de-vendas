﻿namespace SistemaVenda.Entity
{
    public partial class Usuario
    {
        public Usuario()
        {
            Venda = new HashSet<Venda>();
        }

        public int UsuarioId { get; set; }
        public string? Nome { get; set; }
        public string? Email { get; set; }
        public string? Telefone { get; set; }
        public int? RoleId { get; set; }
        public string? UrlFoto { get; set; }
        public string? NomeFoto { get; set; }
        public string? Senha { get; set; }
        public bool? IsAtivo { get; set; }
        public DateTime? DataCadastro { get; set; }

        public virtual Role? Role { get; set; }
        public virtual ICollection<Venda> Venda { get; set; }
    }
}
