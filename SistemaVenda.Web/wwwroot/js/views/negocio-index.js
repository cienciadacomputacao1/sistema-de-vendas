﻿$(document).ready(function () {
    $(".card-body").LoadingOverlay("show");

    fetch("/Negocio/Obter")
        .then(response => {
            $(".card-body").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            console.log(responseJson);
            if (responseJson.estado) {
                const d = responseJson.objeto;

                $("#txtNumeroDocumento").val(d.numeroDocumento);
                $("#txtRazaoSocial").val(d.nome);
                $("#txtEmail").val(d.email);
                $("#txtEndereco").val(d.endereco);
                $("#txtTelefone").val(d.telefone);
                $("#txtImposto").val(d.porcentagemImposto);
                $("#txtSimboloMoeda").val(d.simboloMoeda);
                $("#imgLogo").attr("src", d.urlLogo);
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
})

$("#btnSalvar").click(function () {
    const inputs = $("input.input-validar").serializeArray();
    const inputs_vazio = inputs.filter((item) => item.value.trim() === "");

    if (inputs_vazio.length > 0) {
        const mensagem = `Você deve preencher o campo: "${inputs_vazio[0].name}"`;
        toastr.warning("", mensagem);
        $(`input[name="${inputs_vazio[0].name}"]`).focus()
        return;
    }

    const modelo = {
        numeroDocumento: $("#txtNumeroDocumento").val(),
        nome: $("#txtRazaoSocial").val(),
        email: $("#txtEmail").val(),
        endereco: $("#txtEndereco").val(),
        telefone: $("#txtTelefone").val(),
        porcentagemImposto: $("#txtImposto").val(),
        simboloMoeda: $("#txtSimboloMoeda").val()
        /*$("#imgLogo").attr("src", d.urlLogo);*/
    }

    const inputLogo = document.getElementById("txtLogo");

    const formData = new FormData();

    formData.append("logo", inputLogo.files[0]);
    formData.append("modelo", JSON.stringify(modelo));

    $(".card-body").LoadingOverlay("show");

    fetch("/Negocio/Salvar", {
        method: "POST",
        body: formData
    })
        .then(response => {
            $(".card-body").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            debugger;
            if (responseJson.estado) {
                const d = responseJson.objeto;

                $("#imgLogo").attr("src", d.urlLogo)
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
})