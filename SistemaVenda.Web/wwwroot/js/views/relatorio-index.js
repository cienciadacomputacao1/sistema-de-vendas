﻿let tableData;
$(document).ready(function () {
    $.datepicker.setDefaults($.datepicker.regional["pt-BR"])

    $("#txtDataInicio").datepicker({ dateFormat: "dd/mm/yy" })
    $("#txtDataFim").datepicker({ dateFormat: "dd/mm/yy" })

    tableData = $('#tbdata').DataTable({
        responsive: true,
        "ajax": {
            "url": '/Relatorio/RelatorioVenda?dataInicio=01/01/1991&dataFim=01/01/1991',
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "dataCadastro" },
            { "data": "numeroVenda" },
            { "data": "tipoDocumento" },
            { "data": "documentoCliente" },
            { "data": "nomeCliente" },
            { "data": "subTotalVenda" },
            { "data": "impostoTotalVenda" },
            { "data": "totalVenda" },
            { "data": "produto" },
            { "data": "quantidade" },
            { "data": "preco" },
            { "data": "total" }
        ],
        order: [[0, "desc"]],
        dom: "Bfrtip",
        buttons: [
            {
                text: 'Exportar Excel',
                extend: 'excelHtml5',
                title: '',
                filename: 'Relatorio-de-vendas'
            }, 'pageLength'
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.11.5/i18n/pt-BR.json"
        },
    });
})

$("#btnBuscar").click(function () {
    if ($("#txtDataInicio").val().trim() === "" || $("#txtDataFim").val().trim() === "") {
        toastr.warning("", "Você deve inserir a data de início e término");
        return;
    }

    let dataInicio = $("#txtDataInicio").val().trim();
    let dataFim = $("#txtDataFim").val().trim();

    let novaUrl = `/Relatorio/RelatorioVenda?dataInicio=${dataInicio}&dataFim=${dataFim}`;

    tableData.ajax.url(novaUrl).load();
})

//dataCadastro	"22/10/2023"
//numeroVenda	"000001"
//tipoDocumento	"Boleto"
//documentoCliente	"2344564"
//nomeCliente	"isabella victoria"
//subTotalVenda	"135514,00"
//impostoTotalVenda	"9486,00"
//totalVenda	"145000,00"
//produto	"Computador PC Core I5, RAM 16gb"
//quantidade	1
//preco	"1450,00"
//total	"1450,00"