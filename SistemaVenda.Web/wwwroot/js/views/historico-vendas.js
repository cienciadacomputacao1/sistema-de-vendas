﻿const viewsPesquisar = {
    pesquisaData: () => {
        $("#txtDataInicio").val("")
        $("#txtDataFim").val("")
        $("#txtNumeroVenda").val("")

        $(".pesquisa-data").show()
        $(".pesquisa-venda").hide()
    }, pesquisaVenda: () => {
        $("#txtDataInicio").val("")
        $("#txtDataFim").val("")
        $("#txtNumeroVenda").val("")

        $(".pesquisa-data").hide()
        $(".pesquisa-venda").show()
    }
}

$(document).ready(function () {
    viewsPesquisar["pesquisaData"]()

    $.datepicker.setDefaults($.datepicker.regional["pt-BR"])

    $("#txtDataInicio").datepicker({ dateFormat: "dd/mm/yy" })
    $("#txtDataFim").datepicker({ dateFormat: "dd/mm/yy" })
})

$("#cboBuscarPor").change(function () {
    if ($("#cboBuscarPor").val() === "data") {
        viewsPesquisar["pesquisaData"]()
    } else {
        viewsPesquisar["pesquisaVenda"]()
    }
})

$("#btnBuscar").click(function () {
    if ($("#cboBuscarPor").val() === "data") {
        if ($("#txtDataInicio").val().trim() === "" || $("#txtDataFim").val().trim() === "") {
            toastr.warning("", "Você deve inserir a data de início e término");
            return;
        }
    } else {
        if ($("#txtNumeroVenda").val().trim() === "") {
            toastr.warning("", "Você deve inserir o número de venda");
            return;
        }
    }

    let numeroVenda = $("#txtNumeroVenda").val();
    let dataInicio = $("#txtDataInicio").val();
    let dataFim = $("#txtDataFim").val();

    $(".card-body").find("div.row").LoadingOverlay("show");

    fetch(`/Venda/Historico?numeroVenda=${numeroVenda}&dataInicio=${dataInicio}&dataFim=${dataFim}`)
        .then(response => {
            $(".card-body").find("div.row").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            $("#tbvenda tbody").html("");

            if (responseJson.length > 0) {
                responseJson.forEach((venda) => {
                    $("#tbvenda tbody").append(
                        $("<tr>").append(
                            $("<td>").text(venda.dataCadastro),
                            $("<td>").text(venda.numeroVenda),
                            $("<td>").text(venda.tipoDocumentoVenda),
                            $("<td>").text(venda.documentoCliente),
                            $("<td>").text(venda.nomeCliente),
                            $("<td>").text(venda.total),
                            $("<td>").append(
                                $("<button>").addClass("btn btn-info btn-sm").append(
                                    $("<i>").addClass("fas fa-eye")
                                ).data("venda", venda)
                            )
                        )
                    )
                })
            }
        })
})

$("#tbvenda tbody").on("click", ".btn-info", function () {
    let d = $(this).data("venda")

    $("#txtDataCadastro").val(d.dataCadastro)
    $("#txtNumVenda").val(d.numeroVenda)
    $("#txtUsuarioCadastro").val(d.usuario)
    $("#txtTipoDocumento").val(d.tipoDocumentoVenda)
    $("#txtDocumentoCliente").val(d.documentoCliente)
    $("#txtNomeCliente").val(d.nomeCliente)
    $("#txtSubTotal").val(d.subTotal)
    $("#txtIGV").val(d.impostoTotal)
    $("#txtTotal").val(d.total)


    $("#tbProdutos tbody").html("");

    d.detalheVenda.forEach((item) => {
        $("#tbProdutos tbody").append(
            $("<tr>").append(
                $("<td>").text(item.descricaoProduto),
                $("<td>").text(item.quantidade),
                $("<td>").text(item.preco),
                $("<td>").text(item.total)
            )
        )
    })

    $("#linkImprimir").attr("href", `/Venda/MostrarPDFVenda?numeroVenda=${d.numeroVenda}`)

    $("#modalData").modal("show");
})