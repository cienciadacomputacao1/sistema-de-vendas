﻿$(document).ready(function () {
    $("div.container-fluid").LoadingOverlay("show");

    fetch("/DashBoard/ObterResumo")
        .then(response => {
            $("div.container-fluid").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.estado) {
                // mostrar dados para cartões
                let d = responseJson.objeto
                $("#totalVenda").text(d.totalVendas);
                $("#totalReceitas").text(`R$ ${d.rendaTotal}`);
                $("#totalProdutos").text(d.totalProdutos);
                $("#totalCategorias").text(d.totalCategorias);

                // obter textos e valores para nosso gráfico de barras
                let barchart_labels;
                let barchart_data;

                if (d.vendasUltimaSemana.length > 0) {
                    barchart_labels = d.vendasUltimaSemana.map((item) => { return item.data });
                    barchart_data = d.vendasUltimaSemana.map((item) => { return item.total });
                } else {
                    barchart_labels = "Sem resultados";
                    barchart_data = [0];
                }

                // obter textos e valores para nosso gráfico de pie
                let piechart_labels;
                let piechart_data;

                if (d.principaisProdutosUltimaSemana.length > 0) {
                    piechart_labels = d.principaisProdutosUltimaSemana.map((item) => { return item.produto });
                    piechart_data = d.principaisProdutosUltimaSemana.map((item) => { return item.quantidade });
                } else {
                    piechart_labels = "Sem resultados";
                    piechart_data = [0];
                }

                // Bar Chart Example
                let controlVenda = document.getElementById("chartVendas");
                let myBarChart = new Chart(controlVenda, {
                    type: 'bar',
                    data: {
                        labels: barchart_labels,
                        datasets: [{
                            label: "Quantidade",
                            backgroundColor: "#4e73df",
                            hoverBackgroundColor: "#2e59d9",
                            borderColor: "#4e73df",
                            data: barchart_data,
                        }],
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false
                                },
                                maxBarThickness: 50,
                            }],
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                    maxTicksLimit: 5
                                }
                            }],
                        },
                    }
                });

                // Pie Chart Example
                let controlProduto = document.getElementById("chartProdutos");
                let myPieChart = new Chart(controlProduto, {
                    type: 'doughnut',
                    data: {
                        labels: piechart_labels,
                        datasets: [{
                            data: piechart_data,
                            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', "#FF785B"],
                            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', "#FF5733"],
                            hoverBorderColor: "rgba(234, 236, 244, 1)",
                        }],
                    },
                    options: {
                        maintainAspectRatio: false,
                        tooltips: {
                            backgroundColor: "rgb(255,255,255)",
                            bodyFontColor: "#858796",
                            borderColor: '#dddfeb',
                            borderWidth: 1,
                            xPadding: 15,
                            yPadding: 15,
                            displayColors: false,
                            caretPadding: 10,
                        },
                        legend: {
                            display: true
                        },
                        cutoutPercentage: 80,
                    },
                });

            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
})