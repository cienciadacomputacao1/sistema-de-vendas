﻿const MODELO_BASE = {
    produtoId: 0,
    codigoBarra: "",
    marca: "",
    descricao: "",
    categoriaId: 0,
    estoque: 0,
    urlImagem: "",
    preco: 0,
    isAtivo: 1
}

let tableData;

$(document).ready(function () {
    fetch("/Categoria/Lista")
        .then(response => {
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.data.length > 0) {
                responseJson.data.forEach((item) => {
                    $("#cboCategoria").append(
                        $("<option>").val(item.categoriaId).text(item.descricao)
                    )
                })
            }
        })

    tableData = $('#tbdata').DataTable({
        responsive: true,
        "ajax": {
            "url": '/Produto/Lista',
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "produtoId", "visible": false, "searchable": false },
            {
                "data": "urlImagem", render: function (data) {
                    return `<img style="height:60px" src=${data} class="rounded mx-auto d-block" />`
                }
            },
            { "data": "codigoBarra" },
            { "data": "marca" },
            { "data": "descricao" },
            { "data": "nomeCategoria" },
            { "data": "estoque" },
            { "data": "preco", render: function (data) { return `<text>R$ ${data}</text>` } },
            {
                "data": "isAtivo", render: function (data) {
                    if (data == 1)
                        return '<span class="badge badge-info">Ativo</span>';
                    else
                        return '<span class="badge badge-danger">Não Ativo</span>';
                }
            },
            {
                "defaultContent": '<button class="btn btn-primary btn-editar btn-sm mr-2"><i class="fas fa-pencil-alt"></i></button>' +
                    '<button class="btn btn-danger btn-excluir btn-sm"><i class="fas fa-trash-alt"></i></button>',
                "orderable": false,
                "searchable": false,
                "width": "80px"
            }
        ],
        order: [[0, "desc"]],
        dom: "Bfrtip",
        buttons: [
            {
                text: 'Exportar Excel',
                extend: 'excelHtml5',
                title: '',
                filename: 'Relatorio-de-produto',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6]
                }
            }, 'pageLength'
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.11.5/i18n/pt-BR.json"
        },
    });
})

function mostrarModal(modelo = MODELO_BASE) {
    $("#txtId").val(modelo.produtoId)
    $("#txtCodigoBarra").val(modelo.codigoBarra)
    $("#txtMarca").val(modelo.marca)
    $("#txtDescricao").val(modelo.descricao)
    $("#cboCategoria").val(modelo.categoriaId == 0 ? $("#cboCategoria option:first").val() : modelo.categoriaId)
    $("#txtEstoque").val(modelo.estoque)
    $("#txtPreco").val(modelo.preco)
    $("#cboEstado").val(modelo.isAtivo)
    $("#txtImagem").val("")
    $("#imgProduto").attr("src", modelo.urlImagem)

    $("#modalData").modal("show")
}

$("#btnNovo").click(function () {
    mostrarModal();
})

$("#btnSalvar").click(function () {
    const inputs = $("input.input-validar").serializeArray();
    const inputs_vazio = inputs.filter((item) => item.value.trim() === "");

    if (inputs_vazio.length > 0) {
        const mensagem = `Você deve preencher o campo: "${inputs_vazio[0].name}"`;
        toastr.warning("", mensagem);
        $(`input[name="${inputs_vazio[0].name}"]`).focus()
        return;
    }

    const modelo = structuredClone(MODELO_BASE);
    modelo["produtoId"] = parseInt($("#txtId").val());
    modelo["codigoBarra"] = $("#txtCodigoBarra").val();
    modelo["marca"] = $("#txtMarca").val();
    modelo["descricao"] = $("#txtDescricao").val();
    modelo["categoriaId"] = $("#cboCategoria").val();
    modelo["estoque"] = $("#txtEstoque").val();
    modelo["preco"] = $("#txtPreco").val();
    modelo["isAtivo"] = $("#cboEstado").val();

    const inputFoto = document.getElementById("txtImagem");

    const formData = new FormData();
    formData.append("imagem", inputFoto.files[0]);
    formData.append("modelo", JSON.stringify(modelo));

    $("#modalData").find("div.modal-content").LoadingOverlay("show");

    if (modelo.produtoId === 0) {
        fetch("/Produto/Criar", {
            method: "POST",
            body: formData
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row.add(responseJson.objeto).draw(false);
                $("#modalData").modal("hide");
                swal("Parabéns!", "O produto foi criado", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    } else {
        fetch("/Produto/Editar", {
            method: "PUT",
            body: formData
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row(linhaSelecionada).data(responseJson.objeto).draw(false);
                linhaSelecionada = null;
                $("#modalData").modal("hide");
                swal("Parabéns!", "O produto foi editado", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    }
})

let linhaSelecionada;
$("#tbdata tbody").on("click", ".btn-editar", function () {
    if ($(this).closest("tr").hasClass("child")) {
        linhaSelecionada = $(this).closest("tr").prev();
    } else {
        linhaSelecionada = $(this).closest("tr");
    }

    const data = tableData.row(linhaSelecionada).data();
    mostrarModal(data);
})

$("#tbdata tbody").on("click", ".btn-excluir", function () {
    let linha;
    if ($(this).closest("tr").hasClass("child")) {
        linha = $(this).closest("tr").prev();
    } else {
        linha = $(this).closest("tr");
    }

    const data = tableData.row(linha).data();
    swal({
        title: "Tem certeza?",
        text: `Deseja excluir o produto "${data.descricao}"`,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sim, excluir",
        cancelButtonText: "Não, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (resposta) {
            if (resposta) {
                $(".showSweetAlert").LoadingOverlay("show");

                fetch(`/Produto/Excluir?idProduto=${data.produtoId}`, {
                    method: "DELETE"
                }).then(response => {
                    $(".showSweetAlert").LoadingOverlay("hide");
                    return response.ok ? response.json() : Promise.reject(response);
                }).then(responseJson => {
                    if (responseJson.estado) {
                        tableData.row(linha).remove().draw();
                        swal("Ótimo!", "O produto foi excluído", "success");
                    } else {
                        swal("Sinto muito :(", responseJson.mensagem, "error");
                    }
                })
            }
        }
    )
})