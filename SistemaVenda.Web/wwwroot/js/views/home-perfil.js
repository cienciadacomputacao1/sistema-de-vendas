﻿$(document).ready(function () {
    $(".container-fluid").LoadingOverlay("show");

    fetch("/Home/ObterUsuario")
        .then(response => {
            $(".container-fluid").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.estado) {
                const d = responseJson.objeto;

                $("#imgFoto").attr("src", d.urlFoto);
                $("#txtNome").val(d.nome);
                $("#txtEmail").val(d.email);
                $("#txtTelefone").val(d.telefone);
                $("#txtRole").val(d.nomeRole);
                
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
})

$("#btnSalvarMudancas").click(function () {
    if ($("#txtEmail").val().trim() === "") {
        toastr.warning("", `Você deve preencher o campo: Email`);
        $("#txtEmail").focus();
        return;
    }

    if ($("#txtTelefone").val().trim() === "") {
        toastr.warning("", `Você deve preencher o campo: Telefone`);
        $("#txtTelefone").focus();
        return;
    }

    swal({
        title: "Deseja salvar as mudanças?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (resposta) {
            if (resposta) {
                $(".showSweetAlert").LoadingOverlay("show");

                let modelo = {
                    email: $("#txtEmail").val().trim(),
                    telefone: $("#txtTelefone").val().trim()
                }

                fetch("/Home/SalvarPerfil", {
                    method: "POST",
                    headers: { "Content-Type": "application/json; charset=utf-8" },
                    body: JSON.stringify(modelo)
                }).then(response => {
                    $(".showSweetAlert").LoadingOverlay("hide");
                    return response.ok ? response.json() : Promise.reject(response);
                }).then(responseJson => {
                    if (responseJson.estado) {
                        
                        swal("Ótimo!", "As mudanças foram salvas", "success");
                    } else {
                        swal("Sinto muito :(", responseJson.mensagem, "error");
                    }
                })
            }
        }
    )
})

$("#btnAlterarSenha").click(function () {
    const inputs = $("input.input-validar").serializeArray();
    const inputs_vazio = inputs.filter((item) => item.value.trim() === "");

    if (inputs_vazio.length > 0) {
        const mensagem = `Você deve preencher o campo: "${inputs_vazio[0].name}"`;
        toastr.warning("", mensagem);
        $(`input[name="${inputs_vazio[0].name}"]`).focus()
        return;
    }

    if ($("#txtSenhaNova").val().trim() != $("#txtConfirmarSenha").val().trim()) {
        toastr.warning("", `As senhas não coincidem`);
        return;
    }

    let modelo = {
        senhaAtual: $("#txtSenhaAtual").val().trim(),
        senhaNova: $("#txtSenhaNova").val().trim()
    }

    fetch("/Home/AlterarSenha", {
        method: "POST",
        headers: { "Content-Type": "application/json; charset=utf-8" },
        body: JSON.stringify(modelo)
    }).then(response => {
        $(".showSweetAlert").LoadingOverlay("hide");
        return response.ok ? response.json() : Promise.reject(response);
    }).then(responseJson => {
        if (responseJson.estado) {

            swal("Ótimo!", "Sua senha foi atualizada", "success");
            $("input.input-validar").val("");
        } else {
            swal("Sinto muito :(", responseJson.mensagem, "error");
        }
    })
})