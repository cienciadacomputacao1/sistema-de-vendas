﻿const MODELO_BASE = {
    usuarioId: 0,
    nome: "",
    email: "",
    telefone: "",
    roleId: 0,
    isAtivo: 1,
    urlFoto: ""
}

let tableData;

$(document).ready(function () {
    fetch("/Usuario/ListaRoles")
        .then(response => {
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.length > 0) {
                responseJson.forEach((item) => {
                    $("#cboRole").append(
                        $("<option>").val(item.roleId).text(item.descricao)
                    )
                })
            }
        })

    tableData = $('#tbdata').DataTable({
        responsive: true,
         "ajax": {
             "url": '/Usuario/Lista',
             "type": "GET",
             "datatype": "json"
         },
         "columns": [
             { "data": "usuarioId", "visible": false, "searchable": false },
             {
                 "data": "urlFoto", render: function (data) {
                     return `<img style="height:60px" src=${data} class="rounded mx-auto d-block" />`
                 }
             },
             { "data": "nome" },
             { "data": "email" },
             { "data": "telefone" },
             { "data": "nomeRole" },
             {
                 "data": "isAtivo", render: function (data) {
                     if (data == 1)
                         return '<span class="badge badge-info">Ativo</span>';
                     else
                         return '<span class="badge badge-danger">Não Ativo</span>';
                 }
             },
             {
                 "defaultContent": '<button class="btn btn-primary btn-editar btn-sm mr-2"><i class="fas fa-pencil-alt"></i></button>' +
                     '<button class="btn btn-danger btn-excluir btn-sm"><i class="fas fa-trash-alt"></i></button>',
                 "orderable": false,
                 "searchable": false,
                 "width": "80px"
             }
         ],
         order: [[0, "desc"]],
        dom: "Bfrtip",
        buttons: [
            {
                text: 'Exportar Excel',
                extend: 'excelHtml5',
                title: '',
                filename: 'Relatorio-do-usuario',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6]
                }
            }, 'pageLength'
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.11.5/i18n/pt-BR.json"
        },
    });
})

function mostrarModal(modelo = MODELO_BASE) {
    $("#txtId").val(modelo.usuarioId)
    $("#txtNome").val(modelo.nome)
    $("#txtEmail").val(modelo.email)
    $("#txtTelefone").val(modelo.telefone)
    $("#cboRole").val(modelo.roleId == 0 ? $("#cboRole option:first").val() : modelo.roleId)
    $("#cboEstado").val(modelo.isAtivo)
    $("#txtFoto").val("")

    if ($("#txtId").val(modelo.usuarioId) != 0) {
        $("#imgUsuario").attr("src", modelo.urlFoto)
    }

    $("#modalData").modal("show")
}

$("#btnNovo").click(function () {
    mostrarModal();
})

$("#btnSalvar").click(function () {
    const inputs = $("input.input-validar").serializeArray();
    const inputs_vazio = inputs.filter((item) => item.value.trim() === "");

    if (inputs_vazio.length > 0) {
        const mensagem = `Você deve preencher o campo: "${inputs_vazio[0].name}"`;
        toastr.warning("", mensagem);
        $(`input[name="${inputs_vazio[0].name}"]`).focus()
        return;
    }

    const modelo = structuredClone(MODELO_BASE);
    modelo["usuarioId"] = parseInt($("#txtId").val());
    modelo["nome"] = $("#txtNome").val();
    modelo["email"] = $("#txtEmail").val();
    modelo["telefone"] = $("#txtTelefone").val();
    modelo["roleId"] = $("#cboRole").val();
    modelo["isAtivo"] = $("#cboEstado").val();

    const inputFoto = document.getElementById("txtFoto");

    const formData = new FormData();
    formData.append("foto", inputFoto.files[0]);
    formData.append("modelo", JSON.stringify(modelo));

    $("#modalData").find("div.modal-content").LoadingOverlay("show");

    if (modelo.usuarioId === 0) {
        fetch("/Usuario/Criar", {
            method: "POST",
            body: formData
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row.add(responseJson.objeto).draw(false);
                $("#modalData").modal("hide");
                swal("Parabéns!", "O usuário foi criado", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    } else {
        fetch("/Usuario/Editar", {
            method: "PUT",
            body: formData
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row(linhaSelecionada).data(responseJson.objeto).draw(false);
                linhaSelecionada = null;
                $("#modalData").modal("hide");
                swal("Parabéns!", "O usuário foi editado", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    }
})

let linhaSelecionada;
$("#tbdata tbody").on("click", ".btn-editar", function () {
    if ($(this).closest("tr").hasClass("child")) {
        linhaSelecionada = $(this).closest("tr").prev();
    } else {
        linhaSelecionada = $(this).closest("tr");
    }

    const data = tableData.row(linhaSelecionada).data();
    mostrarModal(data);
})

$("#tbdata tbody").on("click", ".btn-excluir", function () {
    let linha;
    if ($(this).closest("tr").hasClass("child")) {
        linha = $(this).closest("tr").prev();
    } else {
        linha = $(this).closest("tr");
    }

    const data = tableData.row(linha).data();
    swal({
        title: "Tem certeza?",
        text: `Deseja excluir o usuário "${data.nome}"`,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sim, excluir",
        cancelButtonText: "Não, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (resposta) {
            if (resposta) {
                $(".showSweetAlert").LoadingOverlay("show");

                fetch(`/Usuario/Excluir?idUsuario=${data.usuarioId}`, {
                    method: "DELETE"
                }).then(response => {
                    $(".showSweetAlert").LoadingOverlay("hide");
                    return response.ok ? response.json() : Promise.reject(response);
                }).then(responseJson => {
                    if (responseJson.estado) {
                        tableData.row(linha).remove().draw();
                        swal("Ótimo!", "O usuário foi excluído", "success");
                    } else {
                        swal("Sinto muito :(", responseJson.mensagem, "error");
                    }
                })
            }
        }
    )
})