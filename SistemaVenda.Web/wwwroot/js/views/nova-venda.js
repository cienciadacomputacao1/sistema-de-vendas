﻿let valorImposto = 0;
let simboloMoeda;
$(document).ready(function () {
    fetch("/Venda/ListaTipoDocumentoVenda")
        .then(response => {
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.length > 0) {
                responseJson.forEach((item) => {
                    $("#cboTipoDocumentoVenda").append(
                        $("<option>").val(item.tipoDocumentoVendaId).text(item.descricao)
                    )
                })
            }
        })

    fetch("/Negocio/Obter")
        .then(response => {
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.estado) {
                const d = responseJson.objeto;
                console.log(d);

                $("#inputGroupSubTotal").text(`Sub Total - ${d.simboloMoeda}`)
                $("#inputGroupIGV").text(`ICMS(${d.porcentagemImposto}% - ${d.simboloMoeda})`)
                $("#inputGroupTotal").text(`Total - ${d.simboloMoeda}`)

                valorImposto = parseFloat(d.porcentagemImposto);
                simboloMoeda = d.simboloMoeda;
            }
        })

    $("#cboBuscarProduto").select2({
        ajax: {
            url: "/Venda/ObterProdutos",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            delay: 250,
            data: function (params) {
                return {
                    buscar: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data.map((item) => ({
                        id: item.produtoId,
                        text: item.descricao,
                        marca: item.marca,
                        categoria: item.nomeCategoria,
                        urlImagem: item.urlImagem,
                        preco: parseFloat(item.preco)
                    }))
                };
            }
        },
        language: "pt-BR",
        placeholder: 'Buscar Produto...',
        minimumInputLength: 1,
        templateResult: formatoResultados
    });
})

function formatoResultados(data) {
    if (data.loading) {
        return data.text;
    }

    var $container = $(
        `<table width="100%">
            <tr>
                <td style="width:60px">
                    <img style="height:60px; width:60px;margin-right:10px;" src="${data.urlImagem}" />
                </td>
                <td>
                    <p style="font-weight:bolder;margin:2px;">${data.marca}</p>
                    <p style="margin:2px;">${data.text}</p>
                </td>
            </tr>
        </table>
        `
    );

    return $container;
}

$(document).on("select2:open", function () {
    document.querySelector(".select2-search__field").focus();
})

let produtosParaVenda = [];
$("#cboBuscarProduto").on("select2:select", function (e) {
    const data = e.params.data;

    let produto_encontrado = produtosParaVenda.filter(p => p.produtoId == data.id);

    if (produto_encontrado.length > 0) {
        $("#cboBuscarProduto").val("").trigger("change");
        toastr.warning("", "O produto já foi adicionado");
        return false;
    }

    swal({
        title: data.marca,
        text: data.text,
        imageUrl: data.urlImagem,
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        inputPlaceholder: "Insira a quantidade"
    },
        function (valor) {
            if (valor === false) return false;

            if (valor === "") {
                toastr.warning("", "Você precisa inserir o valor");
                return false;
            }

            if (isNaN(parseInt(valor))) {
                toastr.warning("", "Você deve inserir um valor numérico");
                return false;
            }

            let produto = {
                produtoId: data.id,
                marcaProduto: data.marca,
                descricaoProduto: data.text,
                categoriaProduto: data.categoria,
                quantidade: parseInt(valor),
                preco: data.preco.toString(),
                total: (parseFloat(valor) * data.preco).toString()
            }

            produtosParaVenda.push(produto);

            mostrarProdutoPreco();
            $("#cboBuscarProduto").val("").trigger("change");
            swal.close()
        }
    )
})

function mostrarProdutoPreco() {
    let total = 0;
    let igv = 0;
    let subtotal = 0;
    let porcentagem = valorImposto / 100;

    $("#tbProduto tbody").html("")

    produtosParaVenda.forEach((item) => {
        total = total + parseFloat(item.total);

        $("#tbProduto tbody").append(
            $("<tr>").append(
                $("<td>").append(
                    $("<button>").addClass("btn btn-danger btn-excluir btn-sm").append(
                        $("<i>").addClass("fas fa-trash-alt")
                    ).data("produtoId", item.produtoId)
                ),
                $("<td>").text(item.descricaoProduto),
                $("<td>").text(item.quantidade),
                $("<td>").text(simboloMoeda + " " + item.preco),
                $("<td>").text(simboloMoeda + " " + item.total)
            )
        )
    })

    subtotal = total / (1 + porcentagem);
    igv = total - subtotal;

    //$("#txtSubTotal").val(subtotal.toFixed(2))
    //$("#txtIGV").val(igv.toFixed(2))
    //$("#txtTotal").val(total.toFixed(2))

    $("#txtSubTotal").val(subtotal.toFixed(0))
    $("#txtIGV").val(igv.toFixed(0))
    $("#txtTotal").val(total.toFixed(0))
}

$(document).on("click", "button.btn-excluir", function () {
    const _produtoId = $(this).data("produtoId")

    produtosParaVenda = produtosParaVenda.filter(p => p.produtoId != _produtoId);

    mostrarProdutoPreco();
})

$("#btnFinalizarVenda").click(function () {
    if (produtosParaVenda.length < 1) {
        toastr.warning("", "Você deve inserir produtos");
        return;
    }

    const vmDetalheVenda = produtosParaVenda;

    const venda = {
        tipoDocumentoVendaId: $("#cboTipoDocumentoVenda").val(),
        documentoCliente: $("#txtDocumentoCliente").val(),
        nomeCliente: $("#txtNomeCliente").val(),
        subtotal: $("#txtSubTotal").val(),
        impostoTotal: $("#txtIGV").val(),
        total: $("#txtTotal").val(),
        detalheVenda: vmDetalheVenda
    }

    $("#btnFinalizarVenda").closest("div.card-body").LoadingOverlay("show");

    debugger;
    console.log(venda);

    fetch("/Venda/CriarVenda", {
        method: "POST",
        headers: { "Content-Type": "application/json; charset=utf-8" },
        body: JSON.stringify(venda)
    })
        .then(response => {
            $("#btnFinalizarVenda").closest("div.card-body").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        })
        .then(responseJson => {
            if (responseJson.estado) {
                produtosParaVenda = [];
                mostrarProdutoPreco();

                $("#txtDocumentoCliente").val("")
                $("#txtNomeCliente").val("")
                $("#cboTipoDocumentoVenda").val($("#cboTipoDocumentoVenda option:first").val())

                swal("Cadastrado!", `número de venda: ${responseJson.objeto.numeroVenda}`, "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
})