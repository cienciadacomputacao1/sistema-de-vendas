﻿const MODELO_BASE = {
    categoriaId: 0,
    descricao: "",
    isAtivo: 1
}

let tableData;

$(document).ready(function () {
    tableData = $('#tbdata').DataTable({
        responsive: true,
        "ajax": {
            "url": '/Categoria/Lista',
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "categoriaId", "visible": false, "searchable": false },
            { "data": "descricao" },
            {
                "data": "isAtivo", render: function (data) {
                    if (data == 1)
                        return '<span class="badge badge-info">Ativo</span>';
                    else
                        return '<span class="badge badge-danger">Não Ativo</span>';
                }
            },
            {
                "defaultContent": '<button class="btn btn-primary btn-editar btn-sm mr-2"><i class="fas fa-pencil-alt"></i></button>' +
                    '<button class="btn btn-danger btn-excluir btn-sm"><i class="fas fa-trash-alt"></i></button>',
                "orderable": false,
                "searchable": false,
                "width": "80px"
            }
        ],
        order: [[0, "desc"]],
        dom: "Bfrtip",
        buttons: [
            {
                text: 'Exportar Excel',
                extend: 'excelHtml5',
                title: '',
                filename: 'Relatorio-de-categoria',
                exportOptions: {
                    columns: [1, 2]
                }
            }, 'pageLength'
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.11.5/i18n/pt-BR.json"
        },
    });
})

function mostrarModal(modelo = MODELO_BASE) {
    $("#txtId").val(modelo.categoriaId)
    $("#txtDescricao").val(modelo.descricao)
    $("#cboEstado").val(modelo.isAtivo)

    $("#modalData").modal("show")
}

$("#btnNovo").click(function () {
    mostrarModal();
})

$("#btnSalvar").click(function () {
    if ($("#txtDescricao").val().trim() === "") {
        toastr.warning("", `Você deve preencher o campo: Descrição`);
        $("#txtDescricao").focus();
        return;
    }

    const modelo = structuredClone(MODELO_BASE);
    modelo["categoriaId"] = parseInt($("#txtId").val());
    modelo["descricao"] = $("#txtDescricao").val();
    modelo["isAtivo"] = $("#cboEstado").val();

    $("#modalData").find("div.modal-content").LoadingOverlay("show");

    if (modelo.categoriaId === 0) {
        fetch("/Categoria/Criar", {
            method: "POST",
            headers: { "Content-Type": "application/json; charset=utf-8" },
            body: JSON.stringify(modelo)
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row.add(responseJson.objeto).draw(false);
                $("#modalData").modal("hide");
                swal("Parabéns!", "A categoria foi criada", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    } else {
        fetch("/Categoria/Editar", {
            method: "PUT",
            headers: { "Content-Type": "application/json; charset=utf-8" },
            body: JSON.stringify(modelo)
        }).then(response => {
            $("#modalData").find("div.modal-content").LoadingOverlay("hide");
            return response.ok ? response.json() : Promise.reject(response);
        }).then(responseJson => {
            if (responseJson.estado) {
                tableData.row(linhaSelecionada).data(responseJson.objeto).draw(false);
                linhaSelecionada = null;
                $("#modalData").modal("hide");
                swal("Parabéns!", "A categoria foi editada", "success");
            } else {
                swal("Sinto muito :(", responseJson.mensagem, "error");
            }
        })
    }
})

let linhaSelecionada;
$("#tbdata tbody").on("click", ".btn-editar", function () {
    if ($(this).closest("tr").hasClass("child")) {
        linhaSelecionada = $(this).closest("tr").prev();
    } else {
        linhaSelecionada = $(this).closest("tr");
    }

    const data = tableData.row(linhaSelecionada).data();
    mostrarModal(data);
})

$("#tbdata tbody").on("click", ".btn-excluir", function () {
    let linha;
    if ($(this).closest("tr").hasClass("child")) {
        linha = $(this).closest("tr").prev();
    } else {
        linha = $(this).closest("tr");
    }

    const data = tableData.row(linha).data();
    swal({
        title: "Tem certeza?",
        text: `Deseja excluir a categoria "${data.descricao}"`,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sim, excluir",
        cancelButtonText: "Não, cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (resposta) {
            if (resposta) {
                $(".showSweetAlert").LoadingOverlay("show");

                fetch(`/Categoria/Excluir?idCategoria=${data.categoriaId}`, {
                    method: "DELETE"
                }).then(response => {
                    $(".showSweetAlert").LoadingOverlay("hide");
                    return response.ok ? response.json() : Promise.reject(response);
                }).then(responseJson => {
                    if (responseJson.estado) {
                        tableData.row(linha).remove().draw();
                        swal("Ótimo!", "A categoria foi excluída", "success");
                    } else {
                        swal("Sinto muito :(", responseJson.mensagem, "error");
                    }
                })
            }
        }
    )
})