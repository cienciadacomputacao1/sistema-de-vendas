﻿namespace SistemaVenda.Web.ViewModels
{
    public class UsuarioViewModel
    {
        public int UsuarioId { get; set; }
        public string? Nome { get; set; }
        public string? Email { get; set; }
        public string? Telefone { get; set; }
        public int? RoleId { get; set; }
        public string? NomeRole { get; set; }
        public string? UrlFoto { get; set; }
        public int? IsAtivo { get; set; }
    }
}
