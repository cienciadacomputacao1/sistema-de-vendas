﻿namespace SistemaVenda.Web.ViewModels
{
    public class ProdutoViewModel
    {
        public int ProdutoId { get; set; }
        public string? CodigoBarra { get; set; }
        public string? Marca { get; set; }
        public string? Descricao { get; set; }
        public int? CategoriaId { get; set; }
        public string? NomeCategoria { get; set; }
        public int? Estoque { get; set; }
        public string? UrlImagem { get; set; }
        public string? NomeImagem { get; set; }
        public string? Preco { get; set; }
        public int? IsAtivo { get; set; }
    }
}
