﻿namespace SistemaVenda.Web.ViewModels
{
    public class ProdutoSemanaViewModel
    {
        public string Produto { get; set; }
        public int Quantidade { get; set; }
    }
}
