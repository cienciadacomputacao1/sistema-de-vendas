﻿namespace SistemaVenda.Web.ViewModels
{
    public class TipoDocumentoVendaViewModel
    {
        public int TipoDocumentoVendaId { get; set; }
        public string? Descricao { get; set; }
    }
}
