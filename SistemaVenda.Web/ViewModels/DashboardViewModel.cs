﻿namespace SistemaVenda.Web.ViewModels
{
    public class DashboardViewModel
    {
        public int TotalVendas { get; set; }
        public string? RendaTotal { get; set; }
        public int TotalProdutos { get; set; }
        public int TotalCategorias { get; set; }

        public List<VendaSemanaViewModel> VendasUltimaSemana { get; set; }
        public List<ProdutoSemanaViewModel> PrincipaisProdutosUltimaSemana { get; set; }
    }
}
