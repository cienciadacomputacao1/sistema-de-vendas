﻿namespace SistemaVenda.Web.ViewModels
{
    public class VendaSemanaViewModel
    {
        public string? Data { get; set; }
        public int Total { get; set; }
    }
}
