﻿namespace SistemaVenda.Web.ViewModels
{
    public class DetalheVendaViewModel
    {
        public int DetalheVendaId { get; set; }
        public int ProdutoId { get; set; }
        public string? MarcaProduto { get; set; }
        public string? DescricaoProduto { get; set; }
        public string? CategoriaProduto { get; set; }
        public int? Quantidade { get; set; }
        public string? Preco { get; set; }
        public string? Total { get; set; }
    }
}
