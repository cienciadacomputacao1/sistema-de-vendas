﻿namespace SistemaVenda.Web.ViewModels
{
    public class VendaViewModel
    {
        public int VendaId { get; set; }
        public string? NumeroVenda { get; set; }
        public int? TipoDocumentoVendaId { get; set; }
        public string? TipoDocumentoVenda { get; set; }
        public int? UsuarioId { get; set; }
        public string? Usuario { get; set; }
        public string? DocumentoCliente { get; set; }
        public string? NomeCliente { get; set; }
        public string? SubTotal { get; set; }
        public string? ImpostoTotal { get; set; }
        public string? Total { get; set; }
        public string? DataCadastro { get; set; }
        public ICollection<DetalheVendaViewModel> DetalheVenda { get; set; }
    }
}
