﻿namespace SistemaVenda.Web.ViewModels
{
    public class UsuarioLoginViewModel
    {
        public string? Email { get; set; }
        public string? Senha { get; set; }
        public bool ManterSessao { get; set; }
    }
}
