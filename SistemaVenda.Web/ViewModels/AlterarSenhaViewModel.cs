﻿namespace SistemaVenda.Web.ViewModels
{
    public class AlterarSenhaViewModel
    {
        public string? SenhaAtual { get; set; }
        public string? SenhaNova { get; set; }
    }
}
