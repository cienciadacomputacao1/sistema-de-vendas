﻿namespace SistemaVenda.Web.ViewModels
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }
        public string? Descricao { get; set; }
    }
}
