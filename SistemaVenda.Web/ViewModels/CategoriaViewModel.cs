﻿namespace SistemaVenda.Web.ViewModels
{
    public class CategoriaViewModel
    {
        public int CategoriaId { get; set; }
        public string? Descricao { get; set; }
        public int IsAtivo { get; set; }
    }
}
