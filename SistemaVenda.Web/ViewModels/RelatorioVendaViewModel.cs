﻿namespace SistemaVenda.Web.ViewModels
{
    public class RelatorioVendaViewModel
    {
        public string? DataCadastro { get; set; }
        public string? NumeroVenda { get; set; }
        public string? TipoDocumento { get; set; }
        public string? DocumentoCliente { get; set; }
        public string? NomeCliente { get; set; }
        public string? SubTotalVenda { get; set; }
        public string? ImpostoTotalVenda { get; set; }
        public string? TotalVenda { get; set; }
        public string? Produto { get; set; }
        public int Quantidade { get; set; }
        public string? Preco { get; set; }
        public string? Total { get; set; }
    }
}
