﻿namespace SistemaVenda.Web.ViewModels
{
    public class MenuViewModel
    {
        public string? Descricao { get; set; }
        public string? Icone { get; set; }
        public string? Controlador { get; set; }
        public string? PaginaAcao { get; set; }

        public ICollection<MenuViewModel> SubMenus { get; set; }
    }
}
