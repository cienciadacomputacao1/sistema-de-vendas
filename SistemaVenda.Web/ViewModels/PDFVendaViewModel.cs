﻿namespace SistemaVenda.Web.ViewModels
{
    public class PDFVendaViewModel
    {
        public NegocioViewModel? Negocio { get; set; }
        public VendaViewModel? Venda { get; set; }
    }
}
