﻿//using System.Reflection;
//using System.Runtime.Loader;

//namespace SistemaVenda.Web.Utilidades.Extensoes
//{
//    public class CustomAssemblyLoadContext : AssemblyLoadContext
//    {
//        public IntPtr LoadUnmanagedLibrary(string absolutePath)
//        {
//            return LoadUnmanagedDll(absolutePath);
//        }
//        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
//        {
//            return LoadUnmanagedDllFromPath(unmanagedDllName);
//        }
//        protected override Assembly Load(AssemblyName assemblyName)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}

using System.Reflection;
using System.Runtime.Loader;

namespace SistemaVenda.Web.Utilidades.Extensoes
{
    public class CustomAssemblyLoader : AssemblyLoadContext
    {
        private static bool IsLoaded { get; set; }
        public CustomAssemblyLoader()
        {
            LoadUnmanagedLibraryFromResources();
        }

        public static void Load()
        {
            if (!IsLoaded)
            {
                _ = new CustomAssemblyLoader();
                IsLoaded = true;
            }
        }

        public IntPtr LoadUnmanagedLibraryFromResources()
        {
            try
            {
                string tempDllLoaderPath = ExtractDll();
                return LoadUnmanagedDll(tempDllLoaderPath);
            }
            catch (Exception)
            {
                throw new NativeDllLoadException();
            }
        }

        private static string ExtractDll()
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            var dll = Array.Find(thisAssembly.GetManifestResourceNames(), x => x.Contains("libwkhtmltox") && x.EndsWith(".dll"));

            var tempDllLoaderPath = Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll");

            if (File.Exists(tempDllLoaderPath)) return tempDllLoaderPath;

            using (Stream stm = Assembly.GetExecutingAssembly().GetManifestResourceStream(dll))
            {
                using Stream outFile = File.Create(tempDllLoaderPath);
                const int sz = 4096;
                byte[] buf = new byte[sz];
                while (true)
                {
                    int nRead = stm.Read(buf, 0, sz);
                    if (nRead < 1)
                        break;
                    outFile.Write(buf, 0, nRead);
                }
            }
            return tempDllLoaderPath;
        }

        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }
    }
}

public class NativeDllLoadException : Exception
{
    public NativeDllLoadException() : base("Unable Load Native Convertor Dll, Check inner exception for more info")
    {

    }
}