﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace SistemaVenda.Web.Utilidades.ViewComponents
{
    public class MenuUsuarioViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            ClaimsPrincipal claimUser = HttpContext.User;

            var nomeUsuario = string.Empty;
            var urlFotoUsuario = string.Empty;

            if (claimUser.Identity.IsAuthenticated)
            {
                nomeUsuario = claimUser.Claims
                    .Where(c => c.Type == ClaimTypes.Name)
                    .Select(c => c.Value)
                    .SingleOrDefault();

                urlFotoUsuario = ((ClaimsIdentity)claimUser.Identity).FindFirst("UrlFoto").Value;
            }

            ViewData["nomeUsuario"] = nomeUsuario;
            ViewData["urlFotoUsuario"] = urlFotoUsuario;

            return View();
        }
    }
}
