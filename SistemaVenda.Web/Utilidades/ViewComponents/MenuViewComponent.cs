﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Web.ViewModels;
using System.Security.Claims;

namespace SistemaVenda.Web.Utilidades.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IMenuService _menuService;
        private readonly IMapper _mapper;

        public MenuViewComponent(IMenuService menuService, IMapper mapper)
        {
            _menuService = menuService;
            _mapper = mapper;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ClaimsPrincipal claimUser = HttpContext.User;
            List<MenuViewModel> listaMenus = new List<MenuViewModel>();

            if (claimUser.Identity.IsAuthenticated)
            {
                var usuarioId = claimUser.Claims
                .Where(c => c.Type == ClaimTypes.NameIdentifier)
                .Select(c => c.Value)
                .SingleOrDefault();

                listaMenus = _mapper.Map<List<MenuViewModel>>(await _menuService.ObterMenus(int.Parse(usuarioId)));
            }
            else
            {
                listaMenus = new List<MenuViewModel> { };
            }

            return View(listaMenus);
        }
    }
}
