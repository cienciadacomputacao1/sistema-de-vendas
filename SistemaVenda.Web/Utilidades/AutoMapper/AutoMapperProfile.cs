﻿using AutoMapper;
using SistemaVenda.Entity;
using SistemaVenda.Web.ViewModels;
using System.Globalization;

namespace SistemaVenda.Web.Utilidades.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region Role
            CreateMap<Role, RoleViewModel>().ReverseMap();
            #endregion

            #region Usuario
            CreateMap<Usuario, UsuarioViewModel>()
                .ForMember(destino =>
                    destino.IsAtivo,
                    opt => opt.MapFrom(origem => origem.IsAtivo == true ? 1 : 0))
                .ForMember(destino =>
                    destino.NomeRole,
                    opt => opt.MapFrom(origem => origem.Role.Descricao));

            CreateMap<UsuarioViewModel, Usuario>()
                .ForMember(destino =>
                    destino.IsAtivo,
                    opt => opt.MapFrom(origem => origem.IsAtivo == 1 ? true : false))
                .ForMember(destino =>
                    destino.Role,
                    opt => opt.Ignore());
            #endregion

            #region Negocio
            CreateMap<Negocio, NegocioViewModel>()
                .ForMember(destino =>
                    destino.PorcentagemImposto,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.PorcentagemImposto.Value, new CultureInfo("pt-BR"))));

            CreateMap<NegocioViewModel, Negocio>()
                .ForMember(destino =>
                    destino.PorcentagemImposto,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.PorcentagemImposto, new CultureInfo("pt-BR"))));
            #endregion

            #region Categoria
            CreateMap<Categoria, CategoriaViewModel>()
                .ForMember(destino =>
                    destino.IsAtivo,
                    opt => opt.MapFrom(origem => origem.IsAtivo == true ? 1 : 0));

            CreateMap<CategoriaViewModel, Categoria>()
                .ForMember(destino =>
                    destino.IsAtivo,
                    opt => opt.MapFrom(origem => origem.IsAtivo == 1 ? true : false));
            #endregion

            #region Produto

            CreateMap<Produto, ProdutoViewModel>()
                .ForMember(destino =>
                    destino.NomeCategoria,
                    opt => opt.MapFrom(origem => origem.Categoria.Descricao));

            CreateMap<ProdutoViewModel, Produto>()
                .ForMember(destino =>
                    destino.Categoria,
                    opt => opt.Ignore());
            //CreateMap<Produto, ProdutoViewModel>()
            //    .ForMember(destino =>
            //        destino.IsAtivo,
            //        opt => opt.MapFrom(origem => origem.IsAtivo == true ? 1 : 0));

            //CreateMap<Produto, ProdutoViewModel>()
            //    .ForMember(destino =>
            //        destino.NomeCategoria,
            //        opt => opt.MapFrom(origem => origem.Categoria.Descricao));

            //CreateMap<Produto, ProdutoViewModel>()
            //    .ForMember(destino =>
            //        destino.Preco,
            //        opt => opt.MapFrom(origem => Convert.ToString(origem.Preco.Value, new CultureInfo("pt-BR"))));

            //CreateMap<ProdutoViewModel, Produto>()
            //    .ForMember(destino =>
            //        destino.IsAtivo,
            //        opt => opt.MapFrom(origem => origem.IsAtivo == 1 ? true : false));

            //CreateMap<ProdutoViewModel, Produto>()
            //    .ForMember(destino =>
            //        destino.Categoria,
            //        opt => opt.Ignore());

            //CreateMap<ProdutoViewModel, Produto>()
            //    .ForMember(destino =>
            //        destino.Preco,
            //        opt => opt.MapFrom(origem => Convert.ToDecimal(origem.Preco, new CultureInfo("pt-BR"))));
            #endregion

            #region TipoDocumentoVenda
            CreateMap<TipoDocumentoVenda, TipoDocumentoVendaViewModel>().ReverseMap();
            #endregion

            #region Venda
            CreateMap<Venda, VendaViewModel>()
                .ForMember(destino =>
                    destino.TipoDocumentoVenda,
                    opt => opt.MapFrom(origem => origem.TipoDocumentoVenda.Descricao))
                .ForMember(destino =>
                    destino.Usuario,
                    opt => opt.MapFrom(origem => origem.Usuario.Nome))
                .ForMember(destino =>
                    destino.SubTotal,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.SubTotal.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.ImpostoTotal,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.ImpostoTotal.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Total,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Total.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.DataCadastro,
                    opt => opt.MapFrom(origem => origem.DataCadastro.Value.ToString("dd/MM/yyyy")));

            CreateMap<VendaViewModel, Venda>()
                .ForMember(destino =>
                    destino.SubTotal,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.SubTotal, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.ImpostoTotal,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.ImpostoTotal, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Total,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.Total, new CultureInfo("pt-BR"))));
            #endregion

            #region DetalheVenda
            CreateMap<DetalheVenda, DetalheVendaViewModel>()
                .ForMember(destino =>
                    destino.Preco,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Preco.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Total,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Total.Value, new CultureInfo("pt-BR"))));

            CreateMap<DetalheVendaViewModel, DetalheVenda>()
                .ForMember(destino =>
                    destino.Preco,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.Preco, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Total,
                    opt => opt.MapFrom(origem => Convert.ToDecimal(origem.Total, new CultureInfo("pt-BR"))));

            CreateMap<DetalheVenda, RelatorioVendaViewModel>()
                .ForMember(destino =>
                    destino.DataCadastro,
                    opt => opt.MapFrom(origem => origem.Venda.DataCadastro.Value.ToString("dd/MM/yyyy")))
                .ForMember(destino =>
                    destino.NumeroVenda,
                    opt => opt.MapFrom(origem => origem.Venda.NumeroVenda))
                .ForMember(destino =>
                    destino.TipoDocumento,
                    opt => opt.MapFrom(origem => origem.Venda.TipoDocumentoVenda.Descricao))
                .ForMember(destino =>
                    destino.DocumentoCliente,
                    opt => opt.MapFrom(origem => origem.Venda.DocumentoCliente))
                .ForMember(destino =>
                    destino.NomeCliente,
                    opt => opt.MapFrom(origem => origem.Venda.NomeCliente))
                .ForMember(destino =>
                    destino.SubTotalVenda,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Venda.SubTotal.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.ImpostoTotalVenda,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Venda.ImpostoTotal.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.TotalVenda,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Venda.Total.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Produto,
                    opt => opt.MapFrom(origem => origem.DescricaoProduto))
                .ForMember(destino =>
                    destino.Preco,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Preco.Value, new CultureInfo("pt-BR"))))
                .ForMember(destino =>
                    destino.Total,
                    opt => opt.MapFrom(origem => Convert.ToString(origem.Total.Value, new CultureInfo("pt-BR"))));
            #endregion

            #region Menu
            CreateMap<Menu, MenuViewModel>()
                .ForMember(destino =>
                    destino.SubMenus,
                    opt => opt.MapFrom(origem => origem.InverseMenuPai));
            #endregion
        }
    }
}
