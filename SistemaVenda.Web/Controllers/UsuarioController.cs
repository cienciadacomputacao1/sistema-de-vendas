﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public UsuarioController(IUsuarioService usuarioService, IRoleService roleService, IMapper mapper)
        {
            _usuarioService = usuarioService;
            _roleService = roleService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ListaRoles()
        {
            List<RoleViewModel> vmListaRoles = _mapper.Map<List<RoleViewModel>>(await _roleService.Lista());

            return StatusCode(StatusCodes.Status200OK, vmListaRoles);
        }

        [HttpGet]
        public async Task<IActionResult> Lista()
        {
            List<UsuarioViewModel> vmListaUsuarios = _mapper.Map<List<UsuarioViewModel>>(await _usuarioService.Lista());

            return StatusCode(StatusCodes.Status200OK, new { data = vmListaUsuarios });
        }

        [HttpPost]
        public async Task<IActionResult> Criar([FromForm] IFormFile foto, [FromForm] string modelo)
        {
            GenericResponse<UsuarioViewModel> gResponse = new GenericResponse<UsuarioViewModel>();

            try
            {
                UsuarioViewModel vmUsuario = JsonConvert.DeserializeObject<UsuarioViewModel>(modelo);

                var nomeFoto = string.Empty;
                Stream fotoStream = null;

                if (foto != null)
                {
                    var nomeEmCodigo = Guid.NewGuid().ToString("N");
                    var extensao = Path.GetExtension(foto.FileName);
                    nomeFoto = string.Concat(nomeEmCodigo, extensao);
                    fotoStream = foto.OpenReadStream();
                }

                var urlModeloEmail = $"{this.Request.Scheme}://{this.Request.Host}/Modelo/EnviarSenha?email=[email]&senha=[senha]";

                Usuario usuario = await _usuarioService.Criar(_mapper.Map<Usuario>(vmUsuario), fotoStream, nomeFoto, urlModeloEmail);

                vmUsuario = _mapper.Map<UsuarioViewModel>(usuario);

                gResponse.Estado = true;
                gResponse.Objeto = vmUsuario;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpPut]
        public async Task<IActionResult> Editar([FromForm] IFormFile foto, [FromForm] string modelo)
        {
            GenericResponse<UsuarioViewModel> gResponse = new GenericResponse<UsuarioViewModel>();

            try
            {
                UsuarioViewModel vmUsuario = JsonConvert.DeserializeObject<UsuarioViewModel>(modelo);

                var nomeFoto = string.Empty;
                Stream fotoStream = null;

                if (foto != null)
                {
                    var nomeEmCodigo = Guid.NewGuid().ToString("N");
                    var extensao = Path.GetExtension(foto.FileName);
                    nomeFoto = string.Concat(nomeEmCodigo, extensao);
                    fotoStream = foto.OpenReadStream();
                }

                Usuario usuario = await _usuarioService.Editar(_mapper.Map<Usuario>(vmUsuario), fotoStream, nomeFoto);

                vmUsuario = _mapper.Map<UsuarioViewModel>(usuario);

                gResponse.Estado = true;
                gResponse.Objeto = vmUsuario;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpDelete]
        public async Task<IActionResult> Excluir(int idUsuario)
        {
            GenericResponse<string> gResponse = new GenericResponse<string>();

            try
            {
                gResponse.Estado = await _usuarioService.Excluir(idUsuario);
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }
    }
}
