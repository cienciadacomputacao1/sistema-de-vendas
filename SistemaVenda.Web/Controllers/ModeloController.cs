﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    public class ModeloController : Controller
    {
        private readonly IMapper _mapper;
        private readonly INegocioService _negocioService;
        private readonly IVendaService _vendaService;

        public ModeloController(IMapper mapper, INegocioService negocioService, IVendaService vendaService)
        {
            _mapper = mapper;
            _negocioService = negocioService;
            _vendaService = vendaService;
        }

        public IActionResult EnviarSenha(string email, string senha)
        {
            ViewData["Email"] = email;
            ViewData["Senha"] = senha;
            ViewData["Url"] = $"{this.Request.Scheme}://{this.Request.Host}";

            return View();
        }

        public async Task<IActionResult> PDFVenda(string numeroVenda)
        {
            VendaViewModel vmVenda = _mapper.Map<VendaViewModel>(await _vendaService.Detalhe(numeroVenda));
            NegocioViewModel vmNegocio = _mapper.Map<NegocioViewModel>(await _negocioService.Obter());

            PDFVendaViewModel modelo = new PDFVendaViewModel();
            modelo.Negocio = vmNegocio;
            modelo.Venda = vmVenda;

            return View(modelo);
        }

        public IActionResult RestaurarSenha(string senha)
        {
            ViewData["Senha"] = senha;

            return View();
        }
    }
}
