﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class NegocioController : Controller
    {
        private readonly IMapper _mapper;
        private readonly INegocioService _negocioService;

        public NegocioController(IMapper mapper, INegocioService negocioService)
        {
            _mapper = mapper;
            _negocioService = negocioService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Obter()
        {
            GenericResponse<NegocioViewModel> gResponse = new GenericResponse<NegocioViewModel>();

            try
            {
                var vmNegocio = _mapper.Map<NegocioViewModel>(await _negocioService.Obter());

                gResponse.Estado = true;
                gResponse.Objeto = vmNegocio;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpPost]
        public async Task<IActionResult> Salvar([FromForm] IFormFile logo, [FromForm] string modelo)
        {
            GenericResponse<NegocioViewModel> gResponse = new GenericResponse<NegocioViewModel>();

            try
            {
                var vmNegocio = JsonConvert.DeserializeObject<NegocioViewModel>(modelo);

                var nomeLogo = string.Empty;
                Stream logoStream = null!;

                if (logo != null)
                {
                    var nomeEmCodigo = Guid.NewGuid().ToString("N");
                    var extension = Path.GetExtension(logo.FileName);
                    nomeLogo = string.Concat(nomeEmCodigo, extension);
                    logoStream = logo.OpenReadStream();
                }

                var negocio = await _negocioService.Salvar(_mapper.Map<Negocio>(vmNegocio), logoStream, nomeLogo);

                vmNegocio = _mapper.Map<NegocioViewModel>(negocio);

                gResponse.Estado = true;
                gResponse.Objeto = vmNegocio;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }
    }
}
