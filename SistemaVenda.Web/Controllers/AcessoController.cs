﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.ViewModels;
using System.Security.Claims;

namespace SistemaVenda.Web.Controllers
{
    public class AcessoController : Controller
    {
        private readonly IUsuarioService _usuarioService;

        public AcessoController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public IActionResult Login()
        {
            ClaimsPrincipal claimUser = HttpContext.User;

            if (claimUser.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UsuarioLoginViewModel modelo)
        {
            Usuario usuario = await _usuarioService.ObterPorCredenciais(modelo.Email, modelo.Senha);

            if (usuario == null)
            {
                ViewData["Mensagem"] = "Email e/ou senha não conferem";
                return View();
            }

            ViewData["Mensagem"] = null;

            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, usuario.Nome),
                new Claim(ClaimTypes.NameIdentifier, usuario.UsuarioId.ToString()),
                new Claim(ClaimTypes.Role, usuario.RoleId.ToString()),
                new Claim("UrlFoto", usuario.UrlFoto)
            };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            AuthenticationProperties authenticationProperties = new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = modelo.ManterSessao,
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authenticationProperties);

            return RedirectToAction("Index", "DashBoard");
        }

        public IActionResult RestaurarSenha()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RestaurarSenha(UsuarioLoginViewModel modelo)
        {
            try
            {
                var urlModeloEmail = $"{this.Request.Scheme}://{this.Request.Host}/Modelo/RestaurarSenha?senha={modelo.Senha}";

                var resultado = await _usuarioService.RestaurarSenha(modelo.Email, urlModeloEmail);

                if (resultado)
                {
                    ViewData["Mensagem"] = "Ótimo! Sua senha foi restaurada. Revise seu e-mail";
                    ViewData["MensagemError"] = null;
                }
                else
                {
                    ViewData["MensagemError"] = "Temos um problema. Por favor tente novamente mais tarde";
                    ViewData["Mensagem"] = null;
                }
            }
            catch (Exception ex)
            {
                ViewData["MensagemError"] = ex.Message;
                ViewData["Mensagem"] = null;
            }

            return View();
        }
    }
}
