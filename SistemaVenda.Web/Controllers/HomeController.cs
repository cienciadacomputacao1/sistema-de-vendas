﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Models;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;
using System.Diagnostics;
using System.Security.Claims;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IMapper _mapper;

        public HomeController(IUsuarioService usuarioService, IMapper mapper)
        {
            _usuarioService = usuarioService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Perfil()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ObterUsuario()
        {
            GenericResponse<UsuarioViewModel> gResponse = new GenericResponse<UsuarioViewModel>();

            try
            {
                ClaimsPrincipal claimUser = HttpContext.User;

                var usuarioId = claimUser.Claims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .Select(c => c.Value)
                    .SingleOrDefault();

                var usuario = _mapper.Map<UsuarioViewModel>(await _usuarioService.ObterPorId(int.Parse(usuarioId)));

                gResponse.Estado = true;
                gResponse.Objeto = usuario;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpPost]
        public async Task<IActionResult> SalvarPerfil([FromBody] UsuarioViewModel modelo)
        {
            GenericResponse<UsuarioViewModel> gResponse = new GenericResponse<UsuarioViewModel>();

            try
            {
                ClaimsPrincipal claimUser = HttpContext.User;

                var usuarioId = claimUser.Claims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .Select(c => c.Value)
                    .SingleOrDefault();

                var entidade = _mapper.Map<Usuario>(modelo);
                entidade.UsuarioId = int.Parse(usuarioId);

                var resultado = await _usuarioService.SalvarPerfil(entidade);

                gResponse.Estado = resultado;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpPost]
        public async Task<IActionResult> AlterarSenha([FromBody] AlterarSenhaViewModel modelo)
        {
            GenericResponse<bool> gResponse = new GenericResponse<bool>();

            try
            {
                ClaimsPrincipal claimUser = HttpContext.User;

                var usuarioId = claimUser.Claims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .Select(c => c.Value)
                    .SingleOrDefault();

                var resultado = await _usuarioService.AlterarSenha(int.Parse(usuarioId), modelo.SenhaAtual, modelo.SenhaNova);

                gResponse.Estado = resultado;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Sair()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "Acesso");
        }
    }
}