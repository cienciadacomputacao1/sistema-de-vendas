﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class DashBoardController : Controller
    {
        private readonly IDashBoardService _dashboardService;

        public DashBoardController(IDashBoardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ObterResumo()
        {
            GenericResponse<DashboardViewModel> gResponse = new GenericResponse<DashboardViewModel>();

            try
            {
                DashboardViewModel vmDashBoard = new DashboardViewModel();

                vmDashBoard.TotalVendas = await _dashboardService.TotalVendasUltimaSemana();
                vmDashBoard.RendaTotal = await _dashboardService.RendaTotalUltimaSemana();
                vmDashBoard.TotalProdutos = await _dashboardService.TotalProdutos();
                vmDashBoard.TotalCategorias = await _dashboardService.TotalCategorias();

                List<VendaSemanaViewModel> listaVendasSemana = new List<VendaSemanaViewModel>();
                List<ProdutoSemanaViewModel> listaProdutosSemana = new List<ProdutoSemanaViewModel>();

                foreach (KeyValuePair<string, int> item in await _dashboardService.VendasUltimaSemana())
                {
                    listaVendasSemana.Add(new VendaSemanaViewModel()
                    {
                        Data = item.Key,
                        Total = item.Value
                    });
                }

                foreach (KeyValuePair<string, int> item in await _dashboardService.ProdutosTopUltimaSemana())
                {
                    listaProdutosSemana.Add(new ProdutoSemanaViewModel()
                    {
                        Produto = item.Key,
                        Quantidade = item.Value
                    });
                }

                vmDashBoard.VendasUltimaSemana = listaVendasSemana;
                vmDashBoard.PrincipaisProdutosUltimaSemana = listaProdutosSemana;

                gResponse.Estado = true;
                gResponse.Objeto = vmDashBoard;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }
    }
}
