﻿using AutoMapper;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;
using System.Security.Claims;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class VendaController : Controller
    {
        private readonly ITipoDocumentoVendaService _tipoDocumentoVendaService;
        private readonly IVendaService _vendaService;
        private readonly IMapper _mapper;
        private readonly IConverter _converter;

        public VendaController(ITipoDocumentoVendaService tipoDocumentoVendaService, IVendaService vendaService, IMapper mapper, IConverter converter)
        {
            _tipoDocumentoVendaService = tipoDocumentoVendaService;
            _vendaService = vendaService;
            _mapper = mapper;
            _converter = converter;
        }

        public IActionResult NovaVenda()
        {
            return View();
        }

        public IActionResult HistoricoVendas()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ListaTipoDocumentoVenda()
        {
            List<TipoDocumentoVendaViewModel> vmListaTipoDocumentos = _mapper.Map<List<TipoDocumentoVendaViewModel>>(await _tipoDocumentoVendaService.Lista());

            return StatusCode(StatusCodes.Status200OK, vmListaTipoDocumentos);
        }

        [HttpGet]
        public async Task<IActionResult> ObterProdutos(string buscar)
        {
            List<ProdutoViewModel> vmListaProdutos = _mapper.Map<List<ProdutoViewModel>>(await _vendaService.ObterProdutos(buscar));

            return StatusCode(StatusCodes.Status200OK, vmListaProdutos);
        }

        [HttpPost]
        public async Task<IActionResult> CriarVenda([FromBody] VendaViewModel modelo)
        {
            GenericResponse<VendaViewModel> gResponse = new GenericResponse<VendaViewModel>();

            try
            {
                ClaimsPrincipal claimUser = HttpContext.User;

                var usuarioId = claimUser.Claims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .Select(c => c.Value)
                    .SingleOrDefault();

                modelo.UsuarioId = int.Parse(usuarioId);

                var venda = await _vendaService.Criar(_mapper.Map<Venda>(modelo));
                modelo = _mapper.Map<VendaViewModel>(venda);

                gResponse.Estado = true;
                gResponse.Objeto = modelo;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpGet]
        public async Task<IActionResult> Historico(string numeroVenda, string dataInicio, string dataFim)
        {
            List<VendaViewModel> vmHistoricoVendaLista = _mapper.Map<List<VendaViewModel>>(await _vendaService.Historico(numeroVenda, dataInicio, dataFim));

            return StatusCode(StatusCodes.Status200OK, vmHistoricoVendaLista);
        }

        public IActionResult MostrarPDFVenda(string numeroVenda)
        {
            var urlModelo = $"{this.Request.Scheme}://{this.Request.Host}/Modelo/PDFVenda?numeroVenda={numeroVenda}";

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = new GlobalSettings()
                {
                    PaperSize = PaperKind.A4,
                    Orientation = Orientation.Portrait,
                },
                Objects =
                {
                    new ObjectSettings()
                    {
                        Page = urlModelo
                    }
                }
            };

            var arquivoPDF = _converter.Convert(pdf);

            return File(arquivoPDF, "application/pdf");
        }
    }
}
