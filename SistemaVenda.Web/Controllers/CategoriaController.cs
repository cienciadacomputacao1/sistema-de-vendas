﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class CategoriaController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICategoriaService _categoriaService;

        public CategoriaController(IMapper mapper, ICategoriaService categoriaService)
        {
            _mapper = mapper;
            _categoriaService = categoriaService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Lista()
        {
            List<CategoriaViewModel> vmCategoriaLista = _mapper.Map<List<CategoriaViewModel>>(await _categoriaService.Lista());

            return StatusCode(StatusCodes.Status200OK, new { data = vmCategoriaLista });
        }

        [HttpPost]
        public async Task<IActionResult> Criar([FromBody] CategoriaViewModel modelo)
        {
            GenericResponse<CategoriaViewModel> gResponse = new GenericResponse<CategoriaViewModel>();

            try
            {
                var categoria = await _categoriaService.Criar(_mapper.Map<Categoria>(modelo));
                modelo = _mapper.Map<CategoriaViewModel>(categoria);

                gResponse.Estado = true;
                gResponse.Objeto = modelo;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK , gResponse);
        }

        [HttpPut]
        public async Task<IActionResult> Editar([FromBody] CategoriaViewModel modelo)
        {
            GenericResponse<CategoriaViewModel> gResponse = new GenericResponse<CategoriaViewModel>();

            try
            {
                var categoria = await _categoriaService.Editar(_mapper.Map<Categoria>(modelo));
                modelo = _mapper.Map<CategoriaViewModel>(categoria);

                gResponse.Estado = true;
                gResponse.Objeto = modelo;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpDelete]
        public async Task<IActionResult> Excluir(int idCategoria)
        {
            GenericResponse<string> gResponse = new GenericResponse<string>();

            try
            {
                gResponse.Estado = await _categoriaService.Excluir(idCategoria);
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }
    }
}
