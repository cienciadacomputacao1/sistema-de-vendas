﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class RelatorioController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IVendaService _vendaService;

        public RelatorioController(IMapper mapper, IVendaService vendaService)
        {
            _mapper = mapper;
            _vendaService = vendaService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> RelatorioVenda(string dataInicio, string dataFim)
        {
            List<RelatorioVendaViewModel> vmLista = _mapper.Map<List<RelatorioVendaViewModel>>(await _vendaService.Relatorio(dataInicio, dataFim));

            return StatusCode(StatusCodes.Status200OK, new { data = vmLista });
        }
    }
}
