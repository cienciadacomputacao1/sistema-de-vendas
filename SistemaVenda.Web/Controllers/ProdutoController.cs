﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaVenda.BLL.Implementacao;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.Entity;
using SistemaVenda.Web.Utilidades.Response;
using SistemaVenda.Web.ViewModels;

namespace SistemaVenda.Web.Controllers
{
    [Authorize]
    public class ProdutoController : Controller
    {
        
        private readonly IProdutoService _produtoService;
        private readonly ICategoriaService _categoriaService;
        private readonly IMapper _mapper;

        public ProdutoController(IProdutoService produtoService, ICategoriaService categoriaService, IMapper mapper)
        {
            _produtoService = produtoService;
            _categoriaService = categoriaService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ListaCategorias()
        {
            List<CategoriaViewModel> vmListaCategorias = _mapper.Map<List<CategoriaViewModel>>(await _categoriaService.Lista());

            return StatusCode(StatusCodes.Status200OK, vmListaCategorias);
        }

        [HttpGet]
        public async Task<IActionResult> Lista()
        {
            List<ProdutoViewModel> vmProdutoLista = _mapper.Map<List<ProdutoViewModel>>(await _produtoService.Lista());

            return StatusCode(StatusCodes.Status200OK, new { data = vmProdutoLista });
        }

        [HttpPost]
        public async Task<IActionResult> Criar([FromForm] IFormFile imagem, [FromForm] string modelo)
        {
            GenericResponse<ProdutoViewModel> gResponse = new GenericResponse<ProdutoViewModel>();

            try
            {
                var vmProduto = JsonConvert.DeserializeObject<ProdutoViewModel>(modelo);

                var nomeImagem = string.Empty;
                Stream imagemStream = null!;

                if (imagem != null)
                {
                    var nomeCodigo = Guid.NewGuid().ToString("N");
                    var extensao = Path.GetExtension(imagem.FileName);
                    nomeImagem = string.Concat(nomeCodigo, extensao);
                    imagemStream = imagem.OpenReadStream();
                }

                var produto = await _produtoService.Criar(_mapper.Map<Produto>(vmProduto), imagemStream, nomeImagem);

                vmProduto = _mapper.Map<ProdutoViewModel>(produto);

                gResponse.Estado = true;
                gResponse.Objeto = vmProduto;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpPut]
        public async Task<IActionResult> Editar([FromForm] IFormFile imagem, [FromForm] string modelo)
        {
            GenericResponse<ProdutoViewModel> gResponse = new GenericResponse<ProdutoViewModel>();

            try
            {
                var vmProduto = JsonConvert.DeserializeObject<ProdutoViewModel>(modelo);

                var nomeImagem = string.Empty;
                Stream imagemStream = null!;

                if (imagem != null)
                {
                    var nomeCodigo = Guid.NewGuid().ToString("N");
                    var extensao = Path.GetExtension(imagem.FileName);
                    nomeImagem = string.Concat(nomeCodigo, extensao);
                    imagemStream = imagem.OpenReadStream();
                }

                var produto = await _produtoService.Editar(_mapper.Map<Produto>(vmProduto), imagemStream, nomeImagem);

                vmProduto = _mapper.Map<ProdutoViewModel>(produto);

                gResponse.Estado = true;
                gResponse.Objeto = vmProduto;
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }

        [HttpDelete]
        public async Task<IActionResult> Excluir(int idProduto)
        {
            GenericResponse<string> gResponse = new GenericResponse<string>();

            try
            {
                gResponse.Estado = await _produtoService.Excluir(idProduto);
            }
            catch (Exception ex)
            {
                gResponse.Estado = false;
                gResponse.Mensagem = ex.Message;
            }

            return StatusCode(StatusCodes.Status200OK, gResponse);
        }
    }
}
