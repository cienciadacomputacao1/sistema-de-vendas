﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SistemaVenda.BLL.Implementacao;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.DBContext;
using SistemaVenda.DAL.Implementacao;
using SistemaVenda.DAL.Interfaces;

namespace SistemaVenda.IOC
{
    public static class Dependencia
    {
        public static void InjetarDependencia(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DB_SISTEMA_DE_VENDASContext>(options =>
            {
                //options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
                options.UseMySql(configuration.GetConnectionString("DefaultConnection"), ServerVersion.AutoDetect(configuration.GetConnectionString("DefaultConnection")));
            });

            services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IVendaRepository, VendaRepository>();

            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IFirebaseService, FirebaseService>();
            services.AddScoped<IUtilidadeService, UtilidadeService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<INegocioService, NegocioService>();
            services.AddScoped<ICategoriaService, CategoriaService>();
            services.AddScoped<IProdutoService, ProdutoService>();
            services.AddScoped<ITipoDocumentoVendaService, TipoDocumentoVendaService>();
            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IDashBoardService, DashBoardService>();
            services.AddScoped<IMenuService, MenuService>();
        }
    }
}
