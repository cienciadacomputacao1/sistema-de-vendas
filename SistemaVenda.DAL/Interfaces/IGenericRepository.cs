﻿using System.Linq.Expressions;

namespace SistemaVenda.DAL.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Obter(Expression<Func<TEntity, bool>> filtro);
        Task<TEntity> Criar(TEntity entidade);
        Task<bool> Editar(TEntity entidade);
        Task<bool> Excluir(TEntity entidade);
        Task<IQueryable<TEntity>> Consultar(Expression<Func<TEntity, bool>> filtro = null!);
    }
}
