﻿using SistemaVenda.Entity;

namespace SistemaVenda.DAL.Interfaces
{
    public interface IVendaRepository : IGenericRepository<Venda>
    {
        Task<Venda> Cadastrar(Venda entity);
        Task<List<DetalheVenda>> Relatorio(DateTime dataInicio, DateTime dataFim);
    }
}
