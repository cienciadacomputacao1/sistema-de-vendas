﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.Entity;

namespace SistemaVenda.DAL.DBContext
{
    public partial class DB_SISTEMA_DE_VENDASContext : DbContext
    {
        public DB_SISTEMA_DE_VENDASContext()
        { }

        public DB_SISTEMA_DE_VENDASContext(DbContextOptions<DB_SISTEMA_DE_VENDASContext> options)
            : base(options)
        { }

        public virtual DbSet<Categoria> Categoria { get; set; } = null!;
        public virtual DbSet<Configuracao> Configuracaos { get; set; } = null!;
        public virtual DbSet<DetalheVenda> DetalheVenda { get; set; } = null!;
        public virtual DbSet<Menu> Menus { get; set; } = null!;
        public virtual DbSet<Negocio> Negocios { get; set; } = null!;
        public virtual DbSet<NumeroCorrelativo> NumeroCorrelativos { get; set; } = null!;
        public virtual DbSet<Produto> Produtos { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<RoleMenu> RoleMenus { get; set; } = null!;
        public virtual DbSet<TipoDocumentoVenda> TipoDocumentoVenda { get; set; } = null!;
        public virtual DbSet<Usuario> Usuarios { get; set; } = null!;
        public virtual DbSet<Venda> Venda { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.HasKey(e => e.CategoriaId)
                    .HasName("PK__Categori__F353C1E54F4A49F0");

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Configuracao>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Configuracao");

                entity.Property(e => e.Propriedade)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Recurso)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DetalheVenda>(entity =>
            {
                entity.HasKey(e => e.DetalheVendaId)
                    .HasName("PK__DetalheV__C52164D92059884D");

                entity.Property(e => e.CategoriaProduto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DescricaoProduto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaProduto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Preco).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Total).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.Venda)
                    .WithMany(p => p.DetalheVenda)
                    .HasForeignKey(d => d.VendaId)
                    .HasConstraintName("FK__DetalheVe__Venda__571DF1D5");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.ToTable("Menu");

                entity.Property(e => e.Controlador)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Icone)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PaginaAcao)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.MenuPai)
                    .WithMany(p => p.InverseMenuPai)
                    .HasForeignKey(d => d.MenuPaiId)
                    .HasConstraintName("FK__Menu__MenuPaiId__37A5467C");
            });

            modelBuilder.Entity<Negocio>(entity =>
            {
                entity.ToTable("Negocio");

                entity.Property(e => e.NegocioId).ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Endereco)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeLogo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroDocumento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PorcentagemImposto).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SimboloMoeda)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Telefone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrlLogo)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NumeroCorrelativo>(entity =>
            {
                entity.ToTable("NumeroCorrelativo");

                entity.Property(e => e.DataAtualizacao).HasColumnType("datetime");

                entity.Property(e => e.Gestao)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Produto>(entity =>
            {
                entity.ToTable("Produto");

                entity.Property(e => e.CodigoBarra)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Marca)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeImagem)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Preco).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.UrlImagem)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.Produtos)
                    .HasForeignKey(d => d.CategoriaId)
                    .HasConstraintName("FK__Produto__Categor__49C3F6B7");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleMenu>(entity =>
            {
                entity.HasKey(e => e.RoleMenuId)
                    .HasName("PK__RoleMenu__8339C1FEBAC957E5");

                entity.ToTable("RoleMenu");

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.RoleMenus)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("FK__RoleMenu__MenuId__3F466844");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleMenus)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__RoleMenu__RoleId__3E52440B");
            });

            modelBuilder.Entity<TipoDocumentoVenda>(entity =>
            {
                entity.HasKey(e => e.TipoDocumentoVendaId)
                    .HasName("PK__TipoDocu__E013C917D73555AE");

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("Usuario");

                entity.Property(e => e.Senha)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeFoto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Telefone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrlFoto)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Usuario__RoleId__4316F928");
            });

            modelBuilder.Entity<Venda>(entity =>
            {
                entity.HasKey(e => e.VendaId)
                    .HasName("PK__Venda__8020555B0A74D71F");

                entity.Property(e => e.DataCadastro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DocumentoCliente)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ImpostoTotal).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NomeCliente)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroVenda)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.SubTotal).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Total).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.TipoDocumentoVenda)
                    .WithMany(p => p.Venda)
                    .HasForeignKey(d => d.TipoDocumentoVendaId)
                    .HasConstraintName("FK__Venda__TipoDocum__52593CB8");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Venda)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK__Venda__UsuarioId__534D60F1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
