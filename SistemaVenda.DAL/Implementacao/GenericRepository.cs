﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.DAL.DBContext;
using SistemaVenda.DAL.Interfaces;
using System.Linq.Expressions;

namespace SistemaVenda.DAL.Implementacao
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DB_SISTEMA_DE_VENDASContext _dbContext;

        public GenericRepository(DB_SISTEMA_DE_VENDASContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TEntity> Obter(Expression<Func<TEntity, bool>> filtro)
        {
            try
            {
                TEntity entidade = await _dbContext.Set<TEntity>().FirstOrDefaultAsync(filtro);
                return entidade;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> Criar(TEntity entidade)
        {
            try
            {
                _dbContext.Set<TEntity>().Add(entidade);
                await _dbContext.SaveChangesAsync();
                return entidade;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> Editar(TEntity entidade)
        {
            try
            {
                _dbContext.Update(entidade);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> Excluir(TEntity entidade)
        {
            try
            {
                _dbContext.Remove(entidade);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IQueryable<TEntity>> Consultar(Expression<Func<TEntity, bool>> filtro = null!)
        {
            IQueryable<TEntity> queryEntidade = filtro == null ? _dbContext.Set<TEntity>() : _dbContext.Set<TEntity>().Where(filtro);
            return queryEntidade;
        }
    }
}
