﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.DAL.DBContext;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.DAL.Implementacao
{
    public class VendaRepository : GenericRepository<Venda>, IVendaRepository
    {
        private readonly DB_SISTEMA_DE_VENDASContext _dbContext;

        public VendaRepository(DB_SISTEMA_DE_VENDASContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Venda> Cadastrar(Venda entidade)
        {
            var vendaGerada = new Venda();

            using (var transacao = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    foreach (var dv in entidade.DetalheVenda)
                    {
                        var produtoEncontrado = _dbContext.Produtos.Where(p => p.ProdutoId == dv.ProdutoId).First();

                        produtoEncontrado.Estoque -= dv.Quantidade;
                        _dbContext.Produtos.Update(produtoEncontrado);
                    }

                    await _dbContext.SaveChangesAsync();

                    var correlativo = _dbContext.NumeroCorrelativos.Where(n => n.Gestao == "venda").First();

                    correlativo.UltimoNumero += 1;
                    correlativo.DataAtualizacao = DateTime.Now;

                    _dbContext.NumeroCorrelativos.Update(correlativo);
                    await _dbContext.SaveChangesAsync();

                    var zeros = string.Concat(Enumerable.Repeat("0",correlativo.QuantidadeDigitos.Value));
                    var numeroVenda = zeros + correlativo.UltimoNumero.ToString();
                    numeroVenda = numeroVenda.Substring(numeroVenda.Length - correlativo.QuantidadeDigitos.Value, correlativo.QuantidadeDigitos.Value);

                    entidade.NumeroVenda = numeroVenda;

                    await _dbContext.Venda.AddAsync(entidade);
                    await _dbContext.SaveChangesAsync();

                    vendaGerada = entidade;

                    transacao.Commit();
                }
                catch (Exception ex)
                {
                    transacao.Rollback();
                    throw ex;
                }

                return vendaGerada;
            }
        }

        public async Task<List<DetalheVenda>> Relatorio(DateTime dataInicio, DateTime dataFim)
        {
            var listaResumo = await _dbContext.DetalheVenda
                .Include(v => v.Venda)
                .ThenInclude(u => u.Usuario)
                .Include(v => v.Venda)
                .ThenInclude(tdv => tdv.TipoDocumentoVenda)
                .Where(dv => dv.Venda.DataCadastro.Value.Date >= dataInicio.Date &&
                    dv.Venda.DataCadastro.Value.Date <= dataFim.Date)
                .ToListAsync();

            return listaResumo;
        }
    }
}
