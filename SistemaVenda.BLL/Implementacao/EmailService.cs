﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;
using System.Net;
using System.Net.Mail;

namespace SistemaVenda.BLL.Implementacao
{
    public class EmailService : IEmailService
    {
        private readonly IGenericRepository<Configuracao> _configuracaoRepositorio;

        public EmailService(IGenericRepository<Configuracao> configuracaoRepositorio)
        {
            _configuracaoRepositorio = configuracaoRepositorio;
        }

        public async Task<bool> EnviarEmail(string emailDestino, string assunto, string mensagem)
        {
            try
            {
                IQueryable<Configuracao> query = await _configuracaoRepositorio.Consultar(c => c.Recurso.Equals("Servico_Email"));

                Dictionary<string, string> config = query.ToDictionary(keySelector: c => c.Propriedade, elementSelector: c => c.Valor);

                var credenciais = new NetworkCredential(config["email"], config["senha"]);

                var email = new MailMessage()
                {
                    From = new MailAddress(config["email"], config["alias"]),
                    Subject = assunto,
                    Body = mensagem,
                    IsBodyHtml = true
                };

                email.To.Add(new MailAddress(emailDestino));

                var clienteServidor = new SmtpClient()
                {
                    Host = config["host"],
                    Port = int.Parse(config["porta"]),
                    Credentials = credenciais,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    EnableSsl = true
                };

                clienteServidor.Send(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
