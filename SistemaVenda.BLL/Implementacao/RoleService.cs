﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class RoleService : IRoleService
    {
        private readonly IGenericRepository<Role> _roleRepositorio;

        public RoleService(IGenericRepository<Role> roleRepositorio)
        {
            _roleRepositorio = roleRepositorio;
        }

        public async Task<List<Role>> Lista()
        {
            IQueryable<Role> query = await _roleRepositorio.Consultar();

            return query.ToList();
        }
    }
}
