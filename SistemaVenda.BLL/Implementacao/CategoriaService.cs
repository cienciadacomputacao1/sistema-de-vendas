﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class CategoriaService : ICategoriaService
    {
        private readonly IGenericRepository<Categoria> _categoriaRepository;

        public CategoriaService(IGenericRepository<Categoria> categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        public async Task<List<Categoria>> Lista()
        {
            IQueryable<Categoria> query = await _categoriaRepository.Consultar();
            return query.ToList();
        }

        public async Task<Categoria> Criar(Categoria entidade)
        {
            try
            {
                var categoria = await _categoriaRepository.Criar(entidade);

                if (categoria.CategoriaId == 0)
                    throw new TaskCanceledException("Não foi possível criar a categoria");

                return categoria;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Categoria> Editar(Categoria entidade)
        {
            try
            {
                var categoria = await _categoriaRepository.Obter(c => c.CategoriaId == entidade.CategoriaId);

                categoria.Descricao = entidade.Descricao;
                categoria.IsAtivo = entidade.IsAtivo;

                var resposta = await _categoriaRepository.Editar(categoria);

                if (!resposta)
                    throw new TaskCanceledException("Não foi possível alterar a categoria");

                return categoria;
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> Excluir(int idCategoria)
        {
            try
            {
                var categoria = await _categoriaRepository.Obter(c => c.CategoriaId.Equals(idCategoria));

                if (categoria == null)
                    throw new TaskCanceledException("A categoria não existe");

                var resposta = await _categoriaRepository.Excluir(categoria);

                return resposta;
            }
            catch
            {
                throw;
            }
        }
    }
}
