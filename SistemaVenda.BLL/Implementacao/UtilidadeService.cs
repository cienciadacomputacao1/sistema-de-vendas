﻿using SistemaVenda.BLL.Interfaces;
using System.Security.Cryptography;
using System.Text;

namespace SistemaVenda.BLL.Implementacao
{
    public class UtilidadeService : IUtilidadeService
    {
        public string GerarSenha()
        {
            var senha = Guid.NewGuid().ToString("N").Substring(0, 6);
            return senha;
        }

        public string ConverterSha256(string texto)
        {
            var sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;

                byte[] result = hash.ComputeHash(enc.GetBytes(texto));

                foreach (byte b in result)
                {
                    sb.Append(b.ToString("x2"));
                }
            }

            return sb.ToString();
        }
    }
}
