﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class TipoDocumentoVendaService : ITipoDocumentoVendaService
    {
        private readonly IGenericRepository<TipoDocumentoVenda> _tipoDocumentoVendarepository;

        public TipoDocumentoVendaService(IGenericRepository<TipoDocumentoVenda> tipoDocumentoVendarepository)
        {
            _tipoDocumentoVendarepository = tipoDocumentoVendarepository;
        }

        public async Task<List<TipoDocumentoVenda>> Lista()
        {
            IQueryable<TipoDocumentoVenda> query = await _tipoDocumentoVendarepository.Consultar();
            return query.ToList();
        }
    }
}
