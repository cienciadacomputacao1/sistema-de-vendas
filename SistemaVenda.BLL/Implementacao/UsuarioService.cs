﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;
using System.Net;
using System.Text;

namespace SistemaVenda.BLL.Implementacao
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IGenericRepository<Usuario> _usuarioRepositorio;
        private readonly IFirebaseService _firebaseService;
        private readonly IEmailService _emailService;
        private readonly IUtilidadeService _utilidadeService;

        public UsuarioService(IGenericRepository<Usuario> usuarioRepositorio, IFirebaseService firebaseService, IEmailService emailService, IUtilidadeService utilidadeService)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _firebaseService = firebaseService;
            _emailService = emailService;
            _utilidadeService = utilidadeService;
        }

        public async Task<List<Usuario>> Lista()
        {
            IQueryable<Usuario> query = await _usuarioRepositorio.Consultar();
            return query
                .Include(r => r.Role)
                .ToList();
        }

        public async Task<Usuario> Criar(Usuario entidade, Stream foto = null!, string nomeFoto = "", string urlModeloEmail = "")
        {
            var usuarioExistente = await _usuarioRepositorio.Obter(u => u.Email == entidade.Email);

            if (usuarioExistente != null)
                throw new TaskCanceledException("O email já existe!");

            try
            {
                var senhaGerada = _utilidadeService.GerarSenha();
                entidade.Senha = _utilidadeService.ConverterSha256(senhaGerada);
                entidade.NomeFoto = nomeFoto;

                if (foto != null)
                {
                    var urlFoto = await _firebaseService.CarregarStorage(foto, "arquivo_usuario", nomeFoto);
                    entidade.UrlFoto = urlFoto;
                }

                var usuarioCriado = await _usuarioRepositorio.Criar(entidade);

                if (usuarioCriado.UsuarioId == 0)
                    throw new TaskCanceledException("Não foi possível criar o usuário");

                if (urlModeloEmail != "")
                {
                    urlModeloEmail = urlModeloEmail.Replace("[email]", usuarioCriado.Email).Replace("[senha]", senhaGerada);
                    var htmlEmail = string.Empty;

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlModeloEmail);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            StreamReader readerStream = null;

                            if (response.CharacterSet == null)
                                readerStream = new StreamReader(dataStream);
                            else
                                readerStream = new StreamReader(dataStream, Encoding.GetEncoding(response.CharacterSet));

                            htmlEmail = readerStream.ReadToEnd();
                            response.Close();
                            readerStream.Close();
                        }
                    }

                    if (htmlEmail != "")
                        await _emailService.EnviarEmail(usuarioCriado.Email, "Conta Criada", htmlEmail);
                }

                IQueryable<Usuario> query = await _usuarioRepositorio.Consultar(u => u.UsuarioId == usuarioCriado.UsuarioId);
                usuarioCriado = query.Include(r => r.Role).First();

                return usuarioCriado;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Usuario> Editar(Usuario entidade, Stream foto = null!, string nomeFoto = "")
        {
            var usuarioExistente = await _usuarioRepositorio.Obter(u => u.Email == entidade.Email && u.UsuarioId == entidade.UsuarioId);

            if (usuarioExistente == null)
                throw new TaskCanceledException("O usuário não existe!");

            try
            {
                IQueryable<Usuario> queryUsuario = await _usuarioRepositorio.Consultar(u => u.UsuarioId == entidade.UsuarioId);

                var usuario = queryUsuario.First();

                usuario.Nome = entidade.Nome;
                usuario.Email = entidade.Email;
                usuario.Telefone = entidade.Telefone;
                usuario.RoleId = entidade.RoleId;
                usuario.IsAtivo = entidade.IsAtivo;

                if (usuario.NomeFoto == "")
                    usuario.NomeFoto = nomeFoto;

                if (foto != null)
                {
                    var urlFoto = await _firebaseService.CarregarStorage(foto, "arquivo_usuario", usuario.NomeFoto);
                    usuario.UrlFoto = urlFoto;
                }

                var resposta = await _usuarioRepositorio.Editar(usuario);

                if (!resposta)
                    throw new TaskCanceledException("O usuário não pôde ser modificado");

                usuario = queryUsuario.Include(r => r.Role).First();

                return usuario;
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> Excluir(int idUsuario)
        {
            var usuario = await _usuarioRepositorio.Obter(u => u.UsuarioId == idUsuario);

            if (usuario == null)
                throw new TaskCanceledException("O usuário não existe!");

            var nomeFoto = usuario.NomeFoto;
            var resposta = await _usuarioRepositorio.Excluir(usuario);

            if (resposta)
                await _firebaseService.ExcluirStorage("arquivo_usuario", nomeFoto);

            return true;
        }

        public async Task<Usuario> ObterPorCredenciais(string email, string senha)
        {
            var senhaEncriptada = _utilidadeService.ConverterSha256(senha);

            var usuario = await _usuarioRepositorio.Obter(u => u.Email.Equals(email) && u.Senha.Equals(senhaEncriptada));

            return usuario;
        }

        public async Task<Usuario> ObterPorId(int idUsuario)
        {
            IQueryable<Usuario> query = await _usuarioRepositorio.Consultar(u => u.UsuarioId.Equals(idUsuario));

            var resultado = query.Include(r => r.Role).FirstOrDefault();

            return resultado;
        }

        public async Task<bool> SalvarPerfil(Usuario entidade)
        {
            try
            {
                var usuario = await _usuarioRepositorio.Obter(u => u.UsuarioId == entidade.UsuarioId);

                if (usuario == null)
                    throw new TaskCanceledException("O usuário não existe!");

                usuario.Email = entidade.Email;
                usuario.Telefone = entidade.Telefone;

                var resposta = await _usuarioRepositorio.Editar(usuario);

                return resposta;
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> AlterarSenha(int idUsuario, string senhaAtual, string senhaNova)
        {
            try
            {
                var usuario = await _usuarioRepositorio.Obter(u => u.UsuarioId == idUsuario);

                if (usuario == null)
                    throw new TaskCanceledException("O usuário não existe!");

                if (usuario.Senha != _utilidadeService.ConverterSha256(senhaAtual))
                    throw new TaskCanceledException("Senha incorreta!");

                usuario.Senha = _utilidadeService.ConverterSha256(senhaNova);

                var resposta = await _usuarioRepositorio.Editar(usuario);

                return resposta;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> RestaurarSenha(string email, string urlModeloEmail)
        {
            try
            {
                var usuario = await _usuarioRepositorio.Obter(u => u.Email == email);

                if (usuario == null)
                    throw new TaskCanceledException("Não encontramos nenhum usuário associado ao e-mail");

                var senhaGerada = _utilidadeService.GerarSenha();
                usuario.Senha = _utilidadeService.ConverterSha256(senhaGerada);

                urlModeloEmail = urlModeloEmail.Replace("[senha]", senhaGerada);
                var htmlEmail = string.Empty;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlModeloEmail);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader readerStream = null;

                        if (response.CharacterSet == null)
                            readerStream = new StreamReader(dataStream);
                        else
                            readerStream = new StreamReader(dataStream, Encoding.GetEncoding(response.CharacterSet));

                        htmlEmail = readerStream.ReadToEnd();
                        response.Close();
                        readerStream.Close();
                    }
                }

                var emailEnviado = false;

                if (htmlEmail != "")
                    emailEnviado = await _emailService.EnviarEmail(email, "Redefinição de senha", htmlEmail);

                if (!emailEnviado)
                    throw new TaskCanceledException("Houve um problema. Por favor tente novamente mais tarde");

                var resposta = await _usuarioRepositorio.Editar(usuario);

                return resposta;
            }
            catch
            {
                throw;
            }
        }
    }
}
