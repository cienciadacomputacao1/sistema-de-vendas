﻿using Firebase.Auth;
using Firebase.Storage;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class FirebaseService : IFirebaseService
    {
        private readonly IGenericRepository<Configuracao> _configuracaoRepositorio;

        public FirebaseService(IGenericRepository<Configuracao> configuracaoRepositorio)
        {
            _configuracaoRepositorio = configuracaoRepositorio;
        }

        public async Task<string> CarregarStorage(Stream streamArquivo, string pastaDestino, string nomeArquivo)
        {
            string urlImagem = string.Empty;

            try
            {
                IQueryable<Configuracao> query = await _configuracaoRepositorio.Consultar(c => c.Recurso.Equals("FireBase_Storage"));

                Dictionary<string, string> config = query.ToDictionary(keySelector: c => c.Propriedade, elementSelector: c => c.Valor);

                var auth = new FirebaseAuthProvider(new FirebaseConfig(config["api_key"]));
                var a = await auth.SignInWithEmailAndPasswordAsync(config["email"], config["senha"]);

                var cancelamento = new CancellationTokenSource();

                var task = new FirebaseStorage(
                    config["rota"],
                    new FirebaseStorageOptions
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                        ThrowOnCancel = true
                    }
                )
                .Child(config[pastaDestino])
                .Child(nomeArquivo)
                .PutAsync(streamArquivo, cancelamento.Token);

                urlImagem = await task;
            }
            catch
            {
                urlImagem = string.Empty;
            }

            return urlImagem;
        }

        public async Task<bool> ExcluirStorage(string pastaDestino, string nomeArquivo)
        {
            try
            {
                IQueryable<Configuracao> query = await _configuracaoRepositorio.Consultar(c => c.Recurso.Equals("FireBase_Storage"));

                Dictionary<string, string> config = query.ToDictionary(keySelector: c => c.Propriedade, elementSelector: c => c.Valor);

                var auth = new FirebaseAuthProvider(new FirebaseConfig(config["api_key"]));
                var a = await auth.SignInWithEmailAndPasswordAsync(config["email"], config["senha"]);

                var cancelamento = new CancellationTokenSource();

                var task = new FirebaseStorage(
                    config["rota"],
                    new FirebaseStorageOptions
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                        ThrowOnCancel = true
                    }
                )
                .Child(config[pastaDestino])
                .Child(nomeArquivo)
                .DeleteAsync();

                await task;

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
