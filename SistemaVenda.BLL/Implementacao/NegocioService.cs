﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class NegocioService : INegocioService
    {
        private readonly IGenericRepository<Negocio> _negocioRepository;
        private readonly IFirebaseService _firebaseService;

        public NegocioService(IGenericRepository<Negocio> negocioRepository, IFirebaseService firebaseService)
        {
            _negocioRepository = negocioRepository;
            _firebaseService = firebaseService;
        }

        public async Task<Negocio> Obter()
        {
            try
            {
                var negocio = await _negocioRepository.Obter(n => n.NegocioId == 1);
                return negocio;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Negocio> Salvar(Negocio entidade, Stream logo = null!, string nomeLogo = null!)
        {
            try
            {
                var negocio = await _negocioRepository.Obter(n => n.NegocioId == 1);

                negocio.NumeroDocumento = entidade.NumeroDocumento;
                negocio.Nome = entidade.Nome;
                negocio.Email = entidade.Email;
                negocio.Endereco = entidade.Endereco;
                negocio.Telefone = entidade.Telefone;
                negocio.PorcentagemImposto = entidade.PorcentagemImposto;
                negocio.SimboloMoeda = entidade.SimboloMoeda;

                negocio.NomeLogo = negocio.NomeLogo == "" ? nomeLogo : negocio.NomeLogo;

                if (logo != null)
                {
                    var urlFoto = await _firebaseService.CarregarStorage(logo, "arquivo_logo", nomeLogo);
                    negocio.UrlLogo = urlFoto;
                }

                await _negocioRepository.Editar(negocio);
                return negocio;
            }
            catch
            {
                throw;
            }
        }
    }
}
