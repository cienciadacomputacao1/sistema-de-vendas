﻿using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class MenuService : IMenuService
    {
        private readonly IGenericRepository<Menu> _menuRepository;
        private readonly IGenericRepository<RoleMenu> _roleMenuRepository;
        private readonly IGenericRepository<Usuario> _usuarioRepository;

        public MenuService(IGenericRepository<Menu> menuRepository, IGenericRepository<RoleMenu> roleMenuRepository, IGenericRepository<Usuario> usuarioRepository)
        {
            _menuRepository = menuRepository;
            _roleMenuRepository = roleMenuRepository;
            _usuarioRepository = usuarioRepository;
        }

        public async Task<List<Menu>> ObterMenus(int usuarioId)
        {
            IQueryable<Usuario> tbUsuario = await _usuarioRepository.Consultar(u => u.UsuarioId == usuarioId);
            IQueryable<RoleMenu> tbRoleMenu = await _roleMenuRepository.Consultar();
            IQueryable<Menu> tbMenu = await _menuRepository.Consultar();

            IQueryable<Menu> menuPai = (from u in tbUsuario
                                        join rm in tbRoleMenu on u.RoleId equals rm.RoleId
                                        join m in tbMenu on rm.MenuId equals m.MenuId
                                        join mPai in tbMenu on m.MenuPaiId equals mPai.MenuPaiId
                                        select mPai).Distinct().AsQueryable();

            IQueryable<Menu> menuFilhos = (from u in tbUsuario
                                           join rm in tbRoleMenu on u.RoleId equals rm.RoleId
                                           join m in tbMenu on rm.MenuId equals m.MenuId
                                           where m.MenuId != m.MenuPaiId
                                           select m).Distinct().AsQueryable();

            List<Menu> listaMenu = (from mpai in menuPai
                                    select new Menu()
                                    {
                                        Descricao = mpai.Descricao,
                                        Icone = mpai.Icone,
                                        Controlador = mpai.Controlador,
                                        PaginaAcao = mpai.PaginaAcao,
                                        InverseMenuPai = (from mfilho in menuFilhos
                                                          where mfilho.MenuPaiId == mpai.MenuId
                                                          select mfilho).ToList()
                                    }).ToList();

            return listaMenu;
        }
    }
}
