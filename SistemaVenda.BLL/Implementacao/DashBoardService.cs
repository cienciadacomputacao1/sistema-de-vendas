﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;
using System.Globalization;

namespace SistemaVenda.BLL.Implementacao
{
    public class DashBoardService : IDashBoardService
    {
        private readonly IVendaRepository _vendaRepository;
        private readonly IGenericRepository<DetalheVenda> _detalheVendaRepository;
        private readonly IGenericRepository<Categoria> _categoriaRepository;
        private readonly IGenericRepository<Produto> _produtoRepository;
        private DateTime DataInicio = DateTime.Now;

        public DashBoardService(
            IVendaRepository vendaRepository, 
            IGenericRepository<DetalheVenda> detalheVendaRepository, 
            IGenericRepository<Categoria> categoriaRepository, 
            IGenericRepository<Produto> produtoRepository)
        {
            _vendaRepository = vendaRepository;
            _detalheVendaRepository = detalheVendaRepository;
            _categoriaRepository = categoriaRepository;
            _produtoRepository = produtoRepository;
            DataInicio = DataInicio.AddDays(-7);
        }

        public async Task<int> TotalVendasUltimaSemana()
        {
            try
            {
                IQueryable<Venda> query = await _vendaRepository.Consultar(v => v.DataCadastro.Value.Date >= DataInicio.Date);

                var total = query.Count();

                return total;
            }
            catch
            {
                throw;
            }
        }

        public async Task<string> RendaTotalUltimaSemana()
        {
            try
            {
                IQueryable<Venda> query = await _vendaRepository.Consultar(v => v.DataCadastro.Value.Date >= DataInicio.Date);

                var resultado = query.Select(v => v.Total).Sum(v => v.Value);

                return Convert.ToString(resultado, new CultureInfo("pt-BR"));
            }
            catch
            {
                throw;
            }
        }

        public async Task<int> TotalProdutos()
        {
            try
            {
                IQueryable<Produto> query = await _produtoRepository.Consultar();

                var total = query.Count();

                return total;
            }
            catch
            {
                throw;
            }
        }

        public async Task<int> TotalCategorias()
        {
            try
            {
                IQueryable<Categoria> query = await _categoriaRepository.Consultar();

                var total = query.Count();

                return total;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Dictionary<string, int>> VendasUltimaSemana()
        {
            try
            {
                IQueryable<Venda> query = await _vendaRepository.Consultar(v => v.DataCadastro.Value.Date >= DataInicio.Date);

                Dictionary<string, int> resultado = query
                    .GroupBy(v => v.DataCadastro.Value.Date)
                    .OrderByDescending(g => g.Key)
                    .Select(dv => new { data = dv.Key.ToString("dd/MM/yyyy"), total = dv.Count() })
                    .ToDictionary(keySelector: r => r.data, elementSelector: r => r.total);

                return resultado;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Dictionary<string, int>> ProdutosTopUltimaSemana()
        {
            try
            {
                IQueryable<DetalheVenda> query = await _detalheVendaRepository.Consultar();

                Dictionary<string, int> resultado = query
                    .Include(v => v.Venda)
                    .Where(dv => dv.Venda.DataCadastro.Value.Date >= DataInicio.Date)
                    .GroupBy(dv => dv.DescricaoProduto)
                    .OrderByDescending(g => g.Count())
                    .Select(dv => new { produto = dv.Key, total = dv.Count() }).Take(4)
                    .ToDictionary(keySelector: r => r.produto, elementSelector: r => r.total);

                return resultado;
            }
            catch
            {
                throw;
            }
        }
    }
}
