﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Implementacao
{
    public class ProdutoService : IProdutoService
    {
        private readonly IGenericRepository<Produto> _produtoRepository;
        private readonly IFirebaseService _firebaseService;

        public ProdutoService(IGenericRepository<Produto> produtoRepository, IFirebaseService firebaseService)
        {
            _produtoRepository = produtoRepository;
            _firebaseService = firebaseService;
        }

        public async Task<List<Produto>> Lista()
        {
            IQueryable<Produto> query = await _produtoRepository.Consultar();
            var produto =  query.Include(c => c.Categoria)
                .ToList();

            return produto;
        }

        public async Task<Produto> Criar(Produto entidade, Stream imagem = null!, string nomeImagem = "")
        {
            var produtoExistente = await _produtoRepository.Obter(p => p.CodigoBarra == entidade.CodigoBarra);

            if (produtoExistente != null)
                throw new TaskCanceledException("O código de barras já existe");

            try
            {
                entidade.NomeImagem = nomeImagem;

                if (imagem != null)
                {
                    var urlImagem = await _firebaseService.CarregarStorage(imagem, "arquivo_produto", nomeImagem);
                    entidade.UrlImagem = urlImagem;
                }

                var produto = await _produtoRepository.Criar(entidade);

                if (produto.ProdutoId == 0)
                    throw new TaskCanceledException("Não foi possível criar o produto");

                IQueryable<Produto> query = await _produtoRepository.Consultar(p => p.ProdutoId == produto.ProdutoId);

                produto = query.Include(c => c.Categoria).First();

                return produto;
            }
            catch
            {
                throw;
            }
        }

        public async Task<Produto> Editar(Produto entidade, Stream imagem = null!, string nomeImagem = "")
        {
            var produtoExistente = await _produtoRepository.Obter(p => p.CodigoBarra == entidade.CodigoBarra && p.ProdutoId != entidade.ProdutoId);

            if (produtoExistente != null)
                throw new TaskCanceledException("O código de barras já existe");

            try
            {
                IQueryable<Produto> queryProduto = await _produtoRepository.Consultar(p => p.ProdutoId == entidade.ProdutoId);
                var produto = queryProduto.First();

                produto.CodigoBarra = entidade.CodigoBarra;
                produto.Marca = entidade.Marca;
                produto.Descricao = entidade.Descricao;
                produto.CategoriaId = entidade.CategoriaId;
                produto.Estoque = entidade.Estoque;
                produto.Preco = entidade.Preco;
                produto.IsAtivo = entidade.IsAtivo;

                if (string.IsNullOrEmpty(produto.NomeImagem))
                {
                    produto.NomeImagem = nomeImagem;
                }

                if (imagem != null)
                {
                    var urlImagem = await _firebaseService.CarregarStorage(imagem, "arquivo_produto", produto.NomeImagem);
                    produto.UrlImagem = urlImagem;
                }

                var resposta = await _produtoRepository.Editar(produto);

                if (!resposta)
                    throw new TaskCanceledException("Não foi possível editar o produto");

                var produtoEditado = queryProduto.Include(c => c.Categoria).First();

                return produtoEditado;
            }
            catch
            {
                throw;
            }
        }

        public async Task<bool> Excluir(int idProduto)
        {
            try
            {
                var produto = await _produtoRepository.Obter(p => p.ProdutoId == idProduto);

                if (produto == null)
                    throw new TaskCanceledException("O produto não existe");

                var nomeImagem = produto.NomeImagem;

                var resposta = await _produtoRepository.Excluir(produto);

                if (resposta)
                    await _firebaseService.ExcluirStorage("arquivo_produto", nomeImagem);

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}
