﻿using Microsoft.EntityFrameworkCore;
using SistemaVenda.BLL.Interfaces;
using SistemaVenda.DAL.Interfaces;
using SistemaVenda.Entity;
using System.Globalization;

namespace SistemaVenda.BLL.Implementacao
{
    public class VendaService : IVendaService
    {
        private readonly IGenericRepository<Produto> _produtoRepository;
        private readonly IVendaRepository _vendaRepository;

        public VendaService(IGenericRepository<Produto> produtoRepository, IVendaRepository vendaRepository)
        {
            _produtoRepository = produtoRepository;
            _vendaRepository = vendaRepository;
        }

        public async Task<List<Produto>> ObterProdutos(string buscar)
        {
            IQueryable<Produto> query = await _produtoRepository.Consultar(
                p => p.IsAtivo == true 
                && p.Estoque > 0 
                && string.Concat(p.CodigoBarra, p.Marca, p.Descricao).Contains(buscar));

            return query.Include(c => c.Categoria).ToList();
        }

        public async Task<Venda> Criar(Venda entidade)
        {
            try
            {
                return await _vendaRepository.Cadastrar(entidade);
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Venda>> Historico(string numeroVenda, string dataInicio, string dataFim)
        {
            IQueryable<Venda> query = await _vendaRepository.Consultar();
            dataInicio = dataInicio is null ? string.Empty : dataInicio;
            dataFim = dataFim is null ? string.Empty : dataFim;

            if (!string.IsNullOrEmpty(dataInicio) && !string.IsNullOrEmpty(dataFim))
            {
                DateTime data_inicio = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", new CultureInfo("pt-BR"));
                DateTime data_fim = DateTime.ParseExact(dataFim, "dd/MM/yyyy", new CultureInfo("pt-BR"));

                return query.Where(v => 
                    v.DataCadastro.Value.Date >= data_inicio.Date && 
                    v.DataCadastro.Value.Date <= data_fim.Date
                )
                    .Include(tdv => tdv.TipoDocumentoVenda)
                    .Include(u => u.Usuario)
                    .Include(dv => dv.DetalheVenda)
                    .ToList();
            }
            else
            {
                return query.Where(v =>
                    v.NumeroVenda == numeroVenda
                )
                    .Include(tdv => tdv.TipoDocumentoVenda)
                    .Include(u => u.Usuario)
                    .Include(dv => dv.DetalheVenda)
                    .ToList();
            }
        }

        public async Task<Venda> Detalhe(string numeroVenda)
        {
            IQueryable<Venda> query = await _vendaRepository.Consultar(v => v.NumeroVenda == numeroVenda);

            return query
                    .Include(tdv => tdv.TipoDocumentoVenda)
                    .Include(u => u.Usuario)
                    .Include(dv => dv.DetalheVenda)
                    .First();
        }

        public async Task<List<DetalheVenda>> Relatorio(string dataInicio, string dataFim)
        {
            DateTime data_inicio = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", new CultureInfo("pt-BR"));
            DateTime data_fim = DateTime.ParseExact(dataFim, "dd/MM/yyyy", new CultureInfo("pt-BR"));

            List<DetalheVenda> lista = await _vendaRepository.Relatorio(data_inicio, data_fim);

            return lista;
        }
    }
}
