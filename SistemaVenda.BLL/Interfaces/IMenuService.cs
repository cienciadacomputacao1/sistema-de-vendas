﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface IMenuService
    {
        Task<List<Menu>> ObterMenus(int usuarioId);
    }
}
