﻿namespace SistemaVenda.BLL.Interfaces
{
    public interface IUtilidadeService
    {
        string GerarSenha();
        string ConverterSha256(string texto);
    }
}
