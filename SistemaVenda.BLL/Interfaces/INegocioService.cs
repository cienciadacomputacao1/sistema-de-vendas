﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface INegocioService
    {
        Task<Negocio> Obter();
        Task<Negocio> Salvar(Negocio entidade, Stream logo = null!, string nomeLogo = null!);
    }
}
