﻿namespace SistemaVenda.BLL.Interfaces
{
    public interface IFirebaseService
    {
        Task<string> CarregarStorage(Stream streamArquivo, string pastaDestino, string nomeArquivo);
        Task<bool> ExcluirStorage(string pastaDestino, string nomeArquivo);
    }
}
