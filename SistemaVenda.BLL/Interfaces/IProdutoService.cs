﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface IProdutoService
    {
        Task<List<Produto>> Lista();
        Task<Produto> Criar(Produto entidade, Stream imagem = null!, string nomeImagem = "");
        Task<Produto> Editar(Produto entidade, Stream imagem = null!, string nomeImagem = "");
        Task<bool> Excluir(int idProduto);
    }
}
