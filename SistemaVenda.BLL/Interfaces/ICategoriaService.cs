﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface ICategoriaService
    {
        Task<List<Categoria>> Lista();
        Task<Categoria> Criar(Categoria entidade);
        Task<Categoria> Editar(Categoria entidade);
        Task<bool> Excluir(int idCategoria);
    }
}
