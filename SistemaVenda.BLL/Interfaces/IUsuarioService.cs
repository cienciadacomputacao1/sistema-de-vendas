﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface IUsuarioService
    {
        Task<List<Usuario>> Lista();
        Task<Usuario> Criar(Usuario entidade, Stream foto = null!, string nomeFoto = "", string urlModeloEmail = "");
        Task<Usuario> Editar(Usuario entidade, Stream foto = null!, string nomeFoto = "");
        Task<bool> Excluir(int idUsuario);
        Task<Usuario> ObterPorCredenciais(string email, string senha);
        Task<Usuario> ObterPorId(int idUsuario);
        Task<bool> SalvarPerfil(Usuario entidade);
        Task<bool> AlterarSenha(int idUsuario, string senhaAtual, string senhaNova);
        Task<bool> RestaurarSenha(string email, string urlModeloEmail);
    }
}
