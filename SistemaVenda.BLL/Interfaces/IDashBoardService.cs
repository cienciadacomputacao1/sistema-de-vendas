﻿namespace SistemaVenda.BLL.Interfaces
{
    public interface IDashBoardService
    {
        Task<int> TotalVendasUltimaSemana();
        Task<string> RendaTotalUltimaSemana();
        Task<int> TotalProdutos();
        Task<int> TotalCategorias();
        Task<Dictionary<string, int>> VendasUltimaSemana();
        Task<Dictionary<string, int>> ProdutosTopUltimaSemana();
    }
}
