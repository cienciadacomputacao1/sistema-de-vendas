﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface ITipoDocumentoVendaService
    {
        Task<List<TipoDocumentoVenda>> Lista();
    }
}
