﻿namespace SistemaVenda.BLL.Interfaces
{
    public interface IEmailService
    {
        Task<bool> EnviarEmail(string emailDestino, string assunto, string mensagem);
    }
}
