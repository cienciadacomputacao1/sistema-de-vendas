﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface IRoleService
    {
        Task<List<Role>> Lista();
    }
}
