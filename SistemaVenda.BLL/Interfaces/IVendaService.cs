﻿using SistemaVenda.Entity;

namespace SistemaVenda.BLL.Interfaces
{
    public interface IVendaService
    {
        Task<List<Produto>> ObterProdutos(string buscar);
        Task<Venda> Criar(Venda entidade);
        Task<List<Venda>> Historico(string numeroVenda, string dataInicio, string dataFim);
        Task<Venda> Detalhe(string numeroVenda);
        Task<List<DetalheVenda>> Relatorio(string dataInicio, string dataFim);
    }
}
